var application = angular.module('serviceMe', ['chart.js','ionic','LocalStorageModule','ngCordova','ServiceMe.Constants','intlpnIonic','ionic-datepicker','ionic-timepicker','angularMoment','ionic-toast','ngMessages','jett.ionic.filter.bar','rzModule','tabSlideBox'])

.run(['$ionicPlatform','Utils', '$location', '$ionicHistory','$state','$rootScope','Session','config','$cordovaPushV5',function($ionicPlatform, Utils, $location, $ionicHistory, $state, $rootScope,Session,config,$cordovaPushV5) {

  $rootScope.user = Session.getItemObject('current_user_data');
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      window.smsInboxPlugin = cordova.require('cordova/plugin/smsinboxplugin');
      var permissions = cordova.plugins.permissions;
      permissions.hasPermission(permissions.RECEIVE_SMS, checkPermissionCallback, null);
      permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);

      function checkPermissionCallback(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            console.warn('RECEIVE_SMS permission is not turned on');
          }

          permissions.requestPermission(
            permissions.RECEIVE_SMS,
            function(status) {
              if(!status.hasPermission) errorCallback();
            },
            errorCallback);
          }
        }
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
      if(window.cordova){
        $cordovaPushV5.initialize(  // important to initialize with the multidevice structure !!
          {
            android: {
              senderID: config.GCM_SENDER_ID,
              sound:true
            }
          }
        ).then(function (result) {
          $cordovaPushV5.onNotification();
          $cordovaPushV5.onError();
          $cordovaPushV5.register().then(function (deviceToken) {
            // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
            console.log("GCM");
            console.log(deviceToken);
            Session.setItem('device_id',deviceToken);
            if (Session.isAuthenticated()) {
              //notify mainCtrl to update user registration id in database for GCM service
              $rootScope.$emit('register_device');
            }
          }, function (err) {
            // handle error
          });
        });


      }
      // to go back history button and exit on which states
      document.addEventListener("deviceready", function () {

        var exitStates = ['/landing', '/protected'];
        $ionicPlatform.registerBackButtonAction(function (event) {
          if (exitStates.indexOf($location.path()) !== -1) {
            navigator.app.exitApp();
          } else if ($ionicHistory.backView() == null) {
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            navigator.app.exitApp();
          }
          else {
            $ionicHistory.goBack();
          }
        }, 100);

      });
      /*
      * Push notification events
      */
      $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {  // use two variables here, event and data !!!

        if (data.additionalData.foreground === false) {
          console.log(data);

          // do something if the app is in foreground while receiving to push - handle in app push
        } else {
          // handle push messages while app is in background or not started
          console.log(data);
        }


        $cordovaPushV5.finish().then(function (result) {
          // OK finished - works only with the dev-next version of pushV5.js in ngCordova as of February 8, 2016
        }, function (err) {
          // handle error
        });
      });

      $rootScope.$on('$cordovaPushV5:errorOccurred', function(event, error) {
        // handle error
        console.log(error);
      });

    });
  }
])
.config(function ($httpProvider, localStorageServiceProvider, $ionicConfigProvider,$ionicFilterBarConfigProvider,config) {
  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
  localStorageServiceProvider
  .setPrefix('serviceMe')
  .setStorageType('localStorage')
  .setNotify(true, true);
  $ionicConfigProvider.views.forwardCache(true)
  $httpProvider.interceptors.push('httpInterceptor');
  $ionicFilterBarConfigProvider.transition('horizontal');
  $ionicFilterBarConfigProvider.theme('dark');


})
