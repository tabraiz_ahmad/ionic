angular.module("ServiceMe.Constants", [])

.constant("config", {

	//developer
	"API_BASE_PATH": "http://52.199.33.182/rest/api/v1",
	'SERVER_URL':'http://52.199.33.182/rest',
	"ENVIROMENT": 'dev',
	"GCM_SENDER_ID":'515827683010',
	"PUSHER_TOKEN":'1012d9da7acc17f3e398',
	"GEO_CODE_API_KEY":"AIzaSyBHplwF0qQxE1l1tddvsj-xarhRPUHFqeM",
	"CLOSE":'http://localhost/callback',
})
.constant("stringConstants", {
	"REFER_FRIEND_TEXT": 'Need help, real aowsome app, try this',
	"REFER_FRIEND_SUBJECT": 'Want Services',
	"REFER_SUPPLIER_TEXT": 'Be a part of serviceme and earn money',
	"REFER_SUPPLIER_SUBJECT": 'Earn Money',
	'COUNTRIES':{
		"ae":"United Arab Emirates"
	},
	'CITIES':{
		"bh":{
			"mn":"Manama"
		},
		"kw":{},
		"om":{},
		"qa":{},
		"sa":{},
		"ae":{
			"db":"Dubai",
			"sh":"Sharjah",
			"rk":"Ras Al Khaima"
		}
	},
	'AREAS':{
		"db":{
			"dso":"Dubai Silicon Oasis",
			"mc":"Media City",
			"ja":"Jebal Ali",
			"km":"Al Karama"
		},
		"sh":{
			"ra": "Rolla",
			"nd": "Al Nada",
			"np": "NAtonal Paints"
		},
		"rk": {
			"df": "Dhayah Fort",
			"jah": "Jazirat al-Hamra"
		}
	}
})
.constant("subcategories",{
	"HOUSE_CLEANING":"house-cleaning",
	"RENT_A_CAR":"rent-a-car",
	"PEST_CONTROL":"pest-control",
	"AC":"ac",
	"PLUMBING":"plumbing",
	"LAUNDRY":"laundry"
});
