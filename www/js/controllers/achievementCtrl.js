application.controller('achievementCtrl',['$scope','$ionicModal','Utils','Client', function ($scope,$ionicModal,Utils,Client) {

  var init = function(){
    $scope.navTitle = "Achievements";
    Utils.showLoading();
    var review_badges = Client.badges();
    review_badges
    .success(function(response){
      Utils.hideLoading();
      $scope.level = response.level;
      hPix = 308 - 77*($scope.level-1) + 'px';
      $scope.meterHeight = {'height': hPix};
    })
    .error(function(error){
      Utils.hideLoading();
      Utils.debug('Unable to fetch your achievments',error);
    });
  }
  $scope.achievmentHelp = function() {
    $ionicModal.fromTemplateUrl('templates/modals/achievmentHelp.html', function (modal) {
  		$scope.modal = modal;
  		$scope.modal.show();
  	}, {
  		scope: $scope
  	});
  }
  $scope.cancel_modal = function(){
  	$scope.modal.remove();
  }

  init();
}]);
