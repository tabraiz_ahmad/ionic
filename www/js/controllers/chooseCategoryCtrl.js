application.controller('chooseCategoryCtrl',['$scope','$state','Offer','Utils','Supplier','$ionicScrollDelegate', function ($scope, $state, Offer, Utils, Supplier, $ionicScrollDelegate) {

  var init = function(){
    $scope.navTitle = "Choose category";
    Utils.showLoading();
    var categories_promise = Offer.findAllCategories();
    categories_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.categories = response;
    })
    .error(function(error){
      Utils.hideLoading();
      Utils.debug('Unable to fetch sub-categories',error);
    });
  }
  $scope.supplierSignUp = function(){
    var selectedCategorieArray = [];
    angular.forEach($scope.categories, function(category, key) {

      angular.forEach(category.subcategories,function(subcategory, key){
        if(subcategory.selected){
          this.push(subcategory.id);
        }
      },selectedCategorieArray)
    });
    Supplier.categories_id = selectedCategorieArray;
    if(selectedCategorieArray.length){
      $state.go('signUp');
    }
  }
  $scope.$watch('search_string', function () {
    $ionicScrollDelegate.scrollTop();
  });
  $scope.construction = function(){
      $state.go('construction');
  }
  $scope.goLogin =  function(){
      $state.go('login',{'type':'supplier'});
  }
  init();
}
]);
