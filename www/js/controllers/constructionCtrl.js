application.controller('constructionCtrl',['$scope','$rootScope','$state','$ionicModal','$ionicHistory','Utils','Supplier','Session', function ($scope,$rootScope,$state,$ionicModal,$ionicHistory,Utils,Supplier,Session) {
  // $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
  //   viewData.enableBack = true;
  // });
  $scope.goBack = function(){
    $ionicHistory.goBack();
  }
  var init = function(){
    $scope.navTitle = "Service Construction Tools";
    $scope.requirements = [];
    $scope.r_template = {
      "title":"",
      "content":[""]
    }
    $scope.requirements.push(angular.copy($scope.r_template));
    $scope.data={};
    if(Session.isAuthenticated()){
      $scope.has_email = true;
    }
  }
  $scope.constructionHelp = function() {
    $ionicModal.fromTemplateUrl('templates/modals/constructionHelp.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  }
  $scope.cancel_modal = function(){
    $scope.modal.remove();
  }
  $scope.addRequirement = function(){
    var new_requirements = $scope.requirements.concat(angular.copy($scope.r_template));
    $scope.requirements = [];
    $scope.requirements.push.apply($scope.requirements,new_requirements );
  }
  $scope.addContent = function(index){
    $scope.requirements[index].content.push("");
  }
  $scope.removeContent = function(p_index,index){
    $scope.requirements[p_index].content.splice(index,1);
  }
  $scope.constructSubmit = function(){
    var data = {
      'category':$scope.data.category,
      'subcategories':$scope.data.subcategories,
      'email':$scope.data.email ? $scope.data.email:$rootScope.user.email,
      'requirements':$scope.requirements
    };
    Utils.showLoading();
    var submit_promise = Supplier.servicetool(data);
    submit_promise.success(function(response){
      Utils.hideLoading();
      Utils.alertPopup("Submited Successfully","Your suggestion submitted, we will get back to you shortly");
      if(Session.isAuthenticated()){
        $state.go('menu.editProfile');
      }else{
        $state.go('login',{'type':'supplier'});
      }
    }).error(function(err){
      Utils.debug(err);
      Utils.hideLoading();
    })
  }
  $scope.removeRequirement = function(index){
    $scope.requirements.splice(index,1);
  }
  init();
}]);
