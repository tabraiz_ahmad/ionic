application.controller('dashboardCtrl',['$scope','$rootScope','$ionicPopup','$ionicFilterBar','Supplier','Utils','Search','Session',function($scope,$rootScope,$ionicPopup,$ionicFilterBar,Supplier,Utils,Search,Session){
  var init = function(){
    $scope.navTitle = 'Dashboard';
    //change in requirement of graph to a new stupid one

    $scope.labels =  [];
    $scope.data = [[]];
    $scope.series = ['AED'];
    $scope.colors = ['#f39c12'];
    $rootScope.user.new_goal = $rootScope.user.goal;
    Utils.showLoading();
    var earning_promise = Supplier.earnings();
    earning_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.achieved_goal = response.achieved_goal;
      $scope.transaction = response.transaction;
      var data_hash = {};
      angular.forEach($scope.transaction, function(value, key) {
        $scope.labels.push(key);
        $scope.data[0].push(value)
      });
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });

    var maxAmout = $rootScope.user.goal?$rootScope.user.goal:5000;
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1'}];

    $scope.options = {
      scales: {
        yAxes: [{
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left',
          ticks: {
            suggestedMax: maxAmout
          }
        }
      ],
    }
  };
};
$scope.showPopup = function() {
  // An elaborate, custom popup
  $rootScope.user.new_goal = $rootScope.user.goal;
  var myPopup = $ionicPopup.show({
    template: '<input type="number" ng-model="user.new_goal">',
    title: 'Enter Monthly Goal',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (!$rootScope.user.new_goal) {
            //don't allow the user to close unless he enters goal

            e.preventDefault();
          } else {
            $rootScope.user.goal = $rootScope.user.new_goal;
            Utils.showLoading();
            var data = {id:$rootScope.user.id,goal:$rootScope.user.new_goal};
            var update_promise = Supplier.update(data);
            update_promise
            .success(function(response){
              Utils.hideLoading();
              Session.setItemObject('current_user_data',response.supplier);
              //$rootScope.user = response.supplier;
              $scope.options = {
                scales: {
                  yAxes: [{
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks: {
                      suggestedMax: $rootScope.user.goal?$rootScope.user.goal:5000
                    }
                  }
                ],
              }
            };
            })
            .error(function(err){
              Utils.hideLoading();
              Utils.debug(err);
            });


          }
        }
      }
    ]
  });
};
$scope.showFilterBar = function(){
  $scope.searchOpen = {value:true};
  $ionicFilterBar.show({
    update: function (filteredItems,searchText) {
      if(searchText){
        var promise_search_text = Search.searchList(searchText);
        promise_search_text
        .success(function(response){
          $scope.searchResult = response;
        })
        .error(function(error){
          Utils.debug('Error while searching',error);
        });
      }
    },
    cancel: function(){
      $scope.searchOpen = {value:false};
      $scope.searchResult = {};
    }
  });
};

init();
}
])
