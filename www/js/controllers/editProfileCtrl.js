application.controller('editProfileCtrl',['$scope','$rootScope','$state','$ionicPopover','$cordovaCamera','$cordovaFileTransfer','$ionicModal','config','Utils','Supplier','Client','User','Session','ionicToast','Offer'
, function ($scope,$rootScope,$state,$ionicPopover,$cordovaCamera,$cordovaFileTransfer,$ionicModal,config,Utils,Supplier,Client,User,Session,ionicToast,Offer) {

  var init = function(){
    $scope.navTitle = "Edit Profile";
    $rootScope.user = Session.getItemObject('current_user_data');
    $scope.c_password = {type:'password',show:false};
    $scope.password = {type:'password',show:false};
    $scope.change={
      current_password:'',
      mobile:$rootScope.user.mobile,
      email:$rootScope.user.email,
      new_password:'',
      new_password_confirmation:''
    }
    if($rootScope.user){
      $scope.setImage($rootScope.user);
      if($rootScope.user.company_name){
        $scope.name = $rootScope.user.company_name;
      }else{
        $scope.name = $rootScope.user.first_name + $rootScope.user.last_name;
      }
    }
    $ionicPopover.fromTemplateUrl('templates/popover/selectImagePopover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });
    if($rootScope.user.type == 'Supplier'){
      initServices();
    }

  };
  function initServices(){
    $scope.setServiceNames();
    Utils.showLoading();
    var categories_promise = Offer.findAllCategories();
    categories_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.categories = response;
      angular.forEach($scope.categories, function(category, key) {

        angular.forEach(category.subcategories,function(subcategory, key){
          if( $scope.my_services.indexOf(subcategory.id) > -1){
            subcategory.selected = true;
          }
        })
      });

    })
    .error(function(error){
      Utils.hideLoading();
      Utils.debug('Unable to fetch sub-categories',error);
    });
  }
  $scope.setServiceNames = function(){
    $scope.my_services = [];
    $scope.serviceNames = "";
    angular.forEach($rootScope.user.services, function(value, key) {
      $scope.serviceNames += value.name +" ,";
      $scope.my_services.push(value.id);
    });
  }
  $scope.cancel_modal = function(){
    $scope.modal.remove();
  };
  $scope.setImage = function(user){
    if(user.image && user.image.url ){
      user.profilePic =   config.SERVER_URL + user.image.url;
    }
  };
  $scope.jobsList = function(){
    $rootScope.go('menu.jobsList');
  };
  $scope.showHideCurrentPassword = function(){
    if($scope.c_password.show){
      $scope.c_password.type = 'password';
      $scope.c_password.show = false;
    }else{
      $scope.c_password.type = 'text';
      $scope.c_password.show = true;
    }
  };
  $scope.showHidepassword = function(){
    if($scope.password.show){
      $scope.password.type = 'password';
      $scope.password.show = false;
    }else{
      $scope.password.type = 'text';
      $scope.password.show = true;
    }
  };

  $scope.takePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 500,
      targetHeight: 500,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {
      $rootScope.user.profilePic = imageURI;
      $scope.uploadImage();
    }, function (err) {
      // An error occured. Show a message to the user
      Utils.debug('camera error',err);
    });
  };

  $scope.choosePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true

    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {
      $rootScope.user.profilePic = imageURI;
      $scope.uploadImage();
    }, function (err) {
      Utils.debug('image pick error',err);
      // An error occured. Show a message to the user
    });

  };
  $scope.uploadImage = function(){
    var options = {
      fileKey: "image",
      chunkedMode: false,
      mimeType: "image/jpeg",
      httpMethod: "PUT"
    };
    options.fileName = $rootScope.user.profilePic.substr($rootScope.user.profilePic.lastIndexOf('/') + 1);
    Utils.showLoading();
    if($rootScope.user.type == 'Supplier'){
      var update_suplier_promise = Supplier.uploadImage($rootScope.user,$rootScope.user.profilePic,options);
      update_suplier_promise.then(function(result) {
        Utils.hideLoading();
        var response = JSON.parse(result.response);
        Session.setItemObject('current_user_data',response.supplier);
        $scope.setImage($rootScope.user);
      }, function(err) {
        Utils.hideLoading();
        Utils.debug("Unable to update image",err);
        $rootScope.user.profilePic = null;
      }, function (progress) {
        // constant progress updates
      });
    }else{
      var update_client_promise = Client.uploadImage($rootScope.user,$rootScope.user.profilePic,options);
      update_client_promise.then(function(result) {
        Utils.hideLoading();
        var response = JSON.parse(result.response);
        Session.setItemObject('current_user_data',response.client);
        $scope.setImage($rootScope.user);
      }, function(err) {
        Utils.hideLoading();
        Utils.debug("Unable to update image",err);
        $rootScope.user.profilePic = null;
      }, function (progress) {
        // constant progress updates
      });
    }

  };
  $scope.updateSupplier = function(){
    var mobile_change = ($scope.change.mobile != $scope.user.mobile);
    var email_change = ($scope.change.email != $scope.user.email);

    if(mobile_change || email_change){

      var data = {id:$rootScope.user.id};
      if(mobile_change){
        data.mobile = $scope.change.mobile;
      }
      if(email_change){
        data.email = $scope.change.email;
      }
      Utils.showLoading();
      var supplier_update_promise = Supplier.update(data);
      supplier_update_promise.success(function(response){
        Utils.hideLoading();
        Session.setItemObject('current_user_data',response.supplier);
      }).error(function(error){
        Utils.hideLoading();
        Utils.debug("unable to send",error);
        Utils.alertPopup("Error",error.message);
      });
    }else{
      $rootScope.go('menu.profile');
    }
    Utils.debug($rootScope.user);
  };
  $scope.openPasswordModal = function(){
    $scope.credentials={};
    $ionicModal.fromTemplateUrl('templates/modals/changePassword.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  };
  $scope.changePassword = function(){
    Utils.showLoading();
    var password_update_promise = User.updatePassword($scope.credentials);
    password_update_promise.success(function(response){
      Utils.hideLoading();
      ionicToast.show(response.message, 'top', false, 3000);
      $scope.cancel_modal();
    }).error(function(err){
      Utils.hideLoading();
      Utils.alertPopup('Invalid Password','Current password mismatch');
    });
  };
  $scope.openCategoryModle = function(){
    $ionicModal.fromTemplateUrl('templates/modals/categories_select.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  }
  $scope.upDateServices = function(){
    var selectedCategorieArray = [];
    angular.forEach($scope.categories, function(category, key) {

      angular.forEach(category.subcategories,function(subcategory, key){
        if(subcategory.selected){
          this.push(subcategory.id);
        }
      },selectedCategorieArray)
    });
    var data = {};
    data.category_ids = selectedCategorieArray;
    data.id = $rootScope.user.id;
    Utils.showLoading();
    var supplier_update_promise = Supplier.update(data);
    supplier_update_promise.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.supplier);
      init();
      $scope.modal.remove();
    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("unable to send",error);
      Utils.alertPopup("Error",error.message);
    });
  };
  $scope.removeProfilepic = function () {
    var data = {
      id: $rootScope.user.id,
      remove_image: true
    };
    Utils.showLoading();
    var delete_image_promise;
    if($rootScope.user.type == 'Supplier'){
      delete_image_promise = Supplier.update(data);
    }else{
      delete_image_promise = Client.update(data);
    }
    delete_image_promise.success(function(response){
      Utils.hideLoading();
      if(response.supplier)
        Session.setItemObject('current_user_data',response.supplier);
      else
        Session.setItemObject('current_user_data',response.client);
    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("unable to send",error);
      Utils.alertPopup("Error",error.message);
    });
  }
  $scope.construction = function(){
      $scope.modal.remove();
      $state.go('menu.construction');
  }
  init();
}
]);
