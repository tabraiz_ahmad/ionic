application.controller('forgotPasswordCtrl',['$scope','$state','User','Utils', function ($scope, $state, User, Utils) {

  var init = function(){
    $scope.password_input = {type:'password',show:false};
    $scope.option={};
    $scope.option.mobile = true;
    $scope.formData={};
  }
  $scope.sendEmail = function(){
    var data = {};
    data.email = $scope.formData.email;
    Utils.showLoading();
    var promise_resetLink = User.forgotPasswordEmail(data);
    if($scope.option.mobile){
      promise_resetLink
      .success(function(response){
        Utils.hideLoading();
        $state.go("resetPassword");
      })
      .error(function(error){
        Utils.hideLoading();
        Utils.alertPopup("","Mobile not found");
      });
    }else{
      promise_resetLink
      .success(function(response){
        Utils.hideLoading();
        Utils.alertPopup("Reset password","Successfully link sent to " + $scope.formData.email);
        $scope.formData.email = "";
      })
      .error(function(error){
        Utils.hideLoading();
        Utils.alertPopup("","Email not found");
      });
    }
  }
  $scope.reset = function(code,password){
    var data = {code:code,password:password};
    Utils.showLoading();
    var promise_reset = User.resetPassword(data);
    promise_reset
    .success(function(response){
      Utils.hideLoading();
      Utils.alertPopup("Password Changed","Your password has been changed Successfully");
      $state.go('login',{type:'Client'});
    })
    .error(function(error){
      Utils.hideLoading();
      Utils.alertPopup("","Code does not match");
    });
  }
  $scope.showHidepassword = function(){
    if($scope.password_input.show){
      $scope.password_input.type = 'password';
      $scope.password_input.show = false;
    }else{
      $scope.password_input.type = 'text';
      $scope.password_input.show = true;
    }
  }
  init();
}
]);
