application.controller('inboxCtrl',['$scope','$state','$rootScope','User','Utils',function($scope,$state,$rootScope, User, Utils){
	var init = function(hideLoading){
		$scope.navTitle = 'Inbox';
		if(!hideLoading)
			Utils.showLoading();
		var notiication_promise = User.notification();
		notiication_promise
		.success(function(response){
			Utils.hideLoading();
			$scope.notifications = response.notifications;
		}).error(function(err){
			Utils.hideLoading();
			Utils.debug(err);
		});
	};
	$scope.doRefresh = function(){
		var notiication_promise = User.notification();
		notiication_promise
		.success(function(response){
			$scope.notifications = response.notifications;
		}).finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
		});
	};
	$scope.readNotification = function(notification,index){
		$scope.notification.inboxCount = 0;
		User.markmsgAsRead(notification.id);
		// mark as read


		$scope.notifications[index].status = 'seen';
		if($scope.notifications[index].type != "invite_cancel"){
			if( $rootScope.user.type == 'Supplier'){
				$state.go('menu.proposalDetail',{id:notification.metadata.offer_id});
			}else{
				$state.go('menu.proposalDetail',{id:notification.metadata.bid_id});
			}
		}
	}
	init();
	$scope.$on('new_update', function(e) {
		init(true);
	});
}
])
