application.controller('invitationsCtrl',['$scope','$state','Offer','Utils', function ($scope,$state,Offer,Utils) {

  var init = function(){
    $scope.navTitle = "Invitations";
    $scope.current_page = 1;
    Utils.showLoading();
    var jobs_list_promise = Offer.invitations(1);
    jobs_list_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.jobsList = response.offers;
      $scope.total_pages = response.total_pages;
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });

  }
  $scope.loadMore = function(page_num){
    $scope.current_page = page_num;
    Utils.showLoading();
    var jobs_list_promise = Offer.invitations(page_num);
    jobs_list_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.jobsList = $scope.jobsList.concat(response.offers);
      $scope.total_pages = response.total_pages;
    }).error(function(err){
      Utils.hideLoading();
    });

  };
  $scope.doRefresh = function() {

    $scope.current_page = 1;
    var jobs_list_promise = Offer.invitations(1);
    jobs_list_promise
    .success(function(response){
      $scope.jobsList = response.offers;
      $scope.total_pages = response.total_pages;
    })
    .finally(function() {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.offerDetails = function(job){
    if(job.my_bid){
      $state.go('menu.proposalDetail',{id:job.id});
    }else {
      $state.go('menu.jobDetail',{category:job.category_slug,id:job.id});
    }
  }

  $scope.saveJob = function(index){
    var id = $scope.jobsList[index].id;
    Utils.showLoading();
    var save_job_promise = Offer.saveJob(id);
    save_job_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.jobsList[index] = response.offer;
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
      Utils.alertPopup("Error","Network Error please try later");
    });
  };
  init();
}]);
