application.controller('jobDetailCtrl',['$scope','$state','$stateParams','$cordovaCamera','$cordovaFileTransfer','$ionicModal','$ionicPopover','$ionicScrollDelegate','Offer','Utils','subcategories','localStorageService', function ($scope,$state,$stateParams,$cordovaCamera,$cordovaFileTransfer,$ionicModal,$ionicPopover,$ionicScrollDelegate,Offer,Utils,subcategories,localStorageService) {
  $scope.dateObject = {
    titleLabel: 'Date',  //Optional
    inputDate: new Date(),  //Optional
    templateType: 'popup', //Optional
    modalHeaderColor: 'bar-dark', //Optional
    modalFooterColor: 'bar-dark', //Optional
    from: new Date(),
    to: new Date(2050,1,1),
    dateFormat: 'dd-MM-yyyy',
    closeOnSelect: true,
    callback: function (val) {
      datePickerCallback(val);
    }
  };
  var id = $stateParams.id;
  var init = function(){
    $scope.navTitle = "Job Details";
    $scope.category = $stateParams.category;
    $scope.bid={};
    $scope.option = {hideDetail: false,successBox : false};
    Utils.showLoading();
    var jobs_promise = Offer.findOffer(id);
    jobs_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.current_offer = response.offer;
    }).error(function(err){
      Utils.hideLoading();

    });
  }
  var datePickerCallback = function (val) {
    if (typeof(val) != 'undefined') {
      $scope.specificDate = new Date(val);
    }
  };
  $scope.openProposal = function(){
    $scope.modalTitle = 'Submit a Proposal';
    if($scope.current_offer.category_slug == subcategories.RENT_A_CAR){
      $ionicPopover.fromTemplateUrl('templates/popover/selectImagePopover.html', {
        scope: $scope,
      }).then(function(popover) {
        $scope.popover = popover;
      });
      $scope.specificDate = new Date();
      $scope.models = {"Audi Q5":"Audi Q5","Audi Q7":"Audi Q7","Audi RS7":"Audi RS7","Audi Q3":"Audi Q3","Audi S7":"Audi S7",
      "BMW i8":"BMW i8","BMW M6":"BMW M6","BMW X5":"BMW X5", "BMW Z4":"BMW Z4","Huracan":"Huracan","Aventador":"Aventador",
      "Asterion":"Asterion","Passat":"Passat","Beetle":"Beetle","Golf":"Golf","Touareq":"Touareq","Tiguan":"Tiguan",
      "Camry":"Camry","Corolla":"Corolla","Rav4":"Rav4","Land Cruiser":"Land Cruiser","Yaris":"Yaris"};

      $scope.years = ['2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016'];
      $ionicModal.fromTemplateUrl('templates/modals/submitCarRentalProposal.html', function (modal) {
        $scope.proposalModal = modal;
        $scope.proposalModal.show();
      }, {
        scope: $scope
      });
    }else{
      $scope.specificDate = new Date();
      $ionicModal.fromTemplateUrl('templates/modals/submitProposal.html', function (modal) {
        $scope.proposalModal = modal;
        $scope.proposalModal.show();
      }, {
        scope: $scope
      });
    }
  };
  $scope.postProposal = function(){
    Utils.showLoading();
    $scope.bid.start_date = $scope.specificDate;
    $scope.bid.offer_id = id;
    var post_proposal_promise = Offer.postProposal($scope.bid);
    post_proposal_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.current_offer.my_bid = response.bid;
      $scope.option.successBox = true;
      $scope.cancel_modal();
      $state.go('menu.proposalDetail',{id:$scope.current_offer.id});
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });
  };
  $scope.cancelProposal = function(){
    confirm_popup_promise = Utils.confirmPopup("Cancel Proposal","Are you sure to cancel this proposal?",'CONFIRM');
    confirm_popup_promise.then(function(res) {
      if(res) {
        Utils.showLoading();
        var promise_update_proposal = Offer.updateProposal({id:$scope.current_offer.my_bid.id,status:'supplier_cancel'});
        promise_update_proposal.success(function(response){

          Utils.hideLoading();
          $scope.option.successBox = false;
          $scope.option.hideDetail = true;
          $scope.current_offer.my_bid = response.bid;
        }).error(function (error){
          Utils.hideLoading();
          Utils.debug("Unable to delete proposal",error)
          Utils.alertPopup('Unable to delete proposal',error.message);
        });

      }
    });
  }
  $scope.cancel_modal = function(){
    $scope.proposalModal.remove();
    $ionicScrollDelegate.scrollTop();
  }
  $scope.cancelSuccessBox = function(){
    $scope.option.successBox = false;
  }
  $scope.cancel_offer = function(){
    confirm_popup_promise = Utils.confirmPopup("Cancel Offer","Are you sure to reject this offer?",'CONFIRM');
    confirm_popup_promise.then(function(res) {
      if(res) {
        Utils.showLoading();
        var promise_cancel_offer = Offer.cancelOffer($scope.current_offer.id);
        promise_cancel_offer.success(function(response){
          Utils.hideLoading();
          console.log(response);
          Utils.showLoading();
          var jobs_promise = Offer.findOffer(id);
          jobs_promise
          .success(function(response){
            Utils.hideLoading();
            $scope.current_offer = response.offer;
          }).error(function(err){
            Utils.hideLoading();

          });
        }).error(function (error){
          Utils.hideLoading();
          Utils.debug("Unable to cancel offer",error)
          Utils.alertPopup('Unable to cancel offer',error.message);
        });

      }
    });
  }

  $scope.takePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 500,
      targetHeight: 500,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {
      $scope.image = imageURI;

    }, function (err) {
      // An error occured. Show a message to the user
      Utils.debug('camera error',err);
    });
  };

  $scope.choosePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true

    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {

      $scope.image = imageURI;
    }, function (err) {
      Utils.debug('image pick error',err);
      // An error occured. Show a message to the user
    });

  };
  $scope.submitCarRentalProposal = function(){
    $scope.bid.offer_id = id;

    var options = {
      fileKey: "image",
      chunkedMode: false,
      mimeType: "image/jpeg",
      httpMethod: "POST"
    };
    options.fileName = $scope.image.substr($scope.image.lastIndexOf('/') + 1);
    options.params = {};
    options.headers = options.headers || {};
    options.headers['token'] = localStorageService.get('token');
    options.params.bid = $scope.bid;
    Utils.showLoading();
      var update_car_image_promise = Offer.postCarRentalProposal($scope.image,options);
      update_car_image_promise.then(function(result) {
        Utils.hideLoading();
        var response = JSON.parse(result.response);
        $scope.current_offer.my_bid = response.bid;
        $scope.option.successBox = true;
        $scope.cancel_modal();
        $state.go('menu.proposalDetail',{id:$scope.current_offer.id});
        console.log(response);
      }, function(err) {
        Utils.hideLoading();
        console.log(err);
      }, function (progress) {
        // constant progress updates
      });

  };
  init();
}]);
