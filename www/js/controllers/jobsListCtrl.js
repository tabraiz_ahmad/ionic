application.controller('jobsListCtrl',['$scope','$state', '$ionicModal','$ionicSideMenuDelegate','$ionicScrollDelegate','Offer','Utils', function ($scope,$state, $ionicModal, $ionicSideMenuDelegate,$ionicScrollDelegate,Offer,Utils) {

  var init = function(){
    $scope.choice = 'A';
    $scope.locations = {1: 'Al Bahriah',2: 'Al Wajeha',3:'Burdubai',
    4: 'Deira',5: 'Dubai Land',6: 'Jabel Ali',
    7: 'Jumeira',8: 'Madinat',9: 'Mushrif',10: 'Ras Al Khor'
  };
  $scope.search_qry = {};
  $scope.slider = {
    minValue: 0,
    maxValue: 5000,
    options: {
      floor: 0,
      ceil: 5000,
      getSelectionBarColor: function(value) {
        return '#f39c12'
      },
      getPointerColor: function(value){
        return '#f39c12'

      },
      translate: function(value, sliderId, label) {
        switch (label) {
          case 'model':
          return '<b>Min price:</b> $' + value;
          case 'high':
          return '<b>Max price:</b> $' + value;
          default:
          return '$' + value
        }
      },
      onChange: function(id, newValue, highValue, pointerType) {
        if($scope.search_qry){
          $scope.search_qry.price_from = newValue;
          $scope.search_qry.price_to = highValue;
        }else{
          $scope.search_qry = {'price_from' : newValue,'price_to' : highValue};
        }
      }
    }
  };

  $scope.numdays = [{id: 0, text: 'Urgent'},
  {id: 1, text: '1 Day'},
  {id: 2, text: '2 Days'},
  {id: 3, text: '3 Days'},
  {id: 4, text: '4 Days'},
  {id: 5, text: '5 Days'},
  {id: 6, text: '6 Days'},
  {id: 7, text: '7 Days'}];
  $scope.current_page = 1;
  Utils.showLoading();
  var jobs_list_promise = Offer.offersList(1,$scope.search_qry);
  jobs_list_promise
  .success(function(response){
    var sub_categories_promise = Offer.allSubCategories();
    sub_categories_promise.success(function(response){
      $scope.subCategories = response;
      Utils.hideLoading();
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });
    $scope.jobsList = response.offers;
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
  });

}
$scope.loadMore = function(page_num){
  console.log("load more");
  $scope.current_page = page_num;
  Utils.showLoading();
  var jobs_list_promise = Offer.offersList(page_num,$scope.search_qry);
  jobs_list_promise
  .success(function(response){
    Utils.hideLoading();
    $scope.jobsList = $scope.jobsList.concat(response.offers);
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
  });


};
$scope.doRefresh = function() {

  $scope.current_page = 1;
  var jobs_list_promise = Offer.offersList(1,$scope.search_qry);
  jobs_list_promise
  .success(function(response){
    $scope.jobsList = response.offers;
    $scope.total_pages = response.total_pages;
  })
  .finally(function() {
    // Stop the ion-refresher from spinning
    $scope.$broadcast('scroll.refreshComplete');
  });
};
$scope.openFilter = function () {
  $ionicModal.fromTemplateUrl('templates/modals/serviceFilter.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
    $scope.modal.show();
  });
};

$scope.toggleSideMenu = function() {
  $ionicSideMenuDelegate.toggleLeft();
};
$scope.close = function () {
  if($scope.modal){
    $scope.modal.remove();
  }
};

$scope.$watch('search_string', function () {
  $ionicScrollDelegate.scrollTop();
});
$scope.getOffer = function(){
  Utils.showLoading();
  var jobs_list_promise = Offer.offersList(1,$scope.search_qry);
  jobs_list_promise.success(function(response){
    Utils.hideLoading();
    $scope.jobsList = response.offers;
    $scope.total_pages = response.total_pages;
    $scope.close();
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
  });

};
$scope.resetFilter = function(search_qry){
  $scope.search_qry={};
  $scope.slider.minValue = 0;
  $scope.slider.maxValue = 5000;
};
$scope.saveJob = function(index){
  var id = $scope.jobsList[index].id;
  Utils.showLoading();
  var save_job_promise = Offer.saveJob(id);
  save_job_promise
  .success(function(response){
    Utils.hideLoading();
    $scope.jobsList[index] = response.offer;
  })
  .error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
    Utils.alertPopup("Error","Network Error please try later");
  });
};

init();
}]);
