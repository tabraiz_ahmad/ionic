application.controller('landingCtrl',['$scope','$state','$rootScope','Session','Offer','Utils','$ionicFilterBar','Search',function($scope,$state, $rootScope, Session,Offer,Utils,$ionicFilterBar,Search){
	var init = function(){
		$scope.navTitle = "<b>Service<span class='theme-color'>Me</span></b>";
		Utils.showLoading();
		var promise_find_categories = Offer.findAllCategories();
		promise_find_categories
		.success(function(response){
			Utils.hideLoading();
			$scope.services = (response);
		})
		.error(function(error){
			Utils.hideLoading();
			Utils.debug('Error while getting service',error);
		});
		Utils.showLoading();
		var promise_find_trending = Offer.trending();
		promise_find_trending
		.success(function(response){
			Utils.hideLoading();
			$scope.trendings = (response);
		})
		.error(function(error){
			Utils.hideLoading();
			Utils.debug('Error while getting service',error);
		});
	}
	$scope.showFilterBar = function(){
		$scope.searchOpen = {value:true};
		$ionicFilterBar.show({
			update: function (filteredItems,searchText) {
				if(searchText){
					var promise_search_text = Search.searchList(searchText);
					promise_search_text
					.success(function(response){
						$scope.searchResult = response;
					})
					.error(function(error){
						Utils.debug('Error while searching',error);
					});
				}
			},
			cancel: function(){
				$scope.searchOpen = {value:false};
				$scope.searchResult = {};
			}
		});
	}
	$scope.openOfferMethod = function(trending){
		Offer.category_id  = trending.id;
		$state.go('menu.offerMethod',{'category':trending.slug});
	}
	init();

}
])
// ui-sref="menu.offerMethod({'category':trending.slug})"
