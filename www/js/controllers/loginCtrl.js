application.controller('loginCtrl',['$scope','$rootScope', '$state', '$stateParams', '$ionicActionSheet','$ionicModal','User', 'Utils','Session','Client',
function($scope,$rootScope, $state, $stateParams, $ionicActionSheet, $ionicModal, User, Utils, Session, Client) {
  var init = function(){
    $scope.navTitle = 'LogIn';
    $scope.type = $stateParams.type;
    $scope.password = {type:'password',show:false};
    $scope.data = {};
  }
  $scope.showHidepassword = function(){
    if($scope.password.show){
      $scope.password.type = 'password';
      $scope.password.show = false;
    }else{
      $scope.password.type = 'text';
      $scope.password.show = true;
    }
  }

  $scope.user = {};
  $scope.fbLogin = function() {
    Utils.showLoading();
    if(typeof facebookConnectPlugin != 'undefined'){
      facebookConnectPlugin.getLoginStatus(function(success) {
        if (success.status === 'connected') {
          // The user is logged in and has authenticated app, and response.authResponse supplies
          // the user's ID, a valid access token, a signed request, and the time the access token
          // and signed request each expire
          var auth_token = success.authResponse.accessToken;
          var auth_promise = User.auth($scope.type,'facebook',auth_token);
          auth_promise.success(function(response){
            Utils.hideLoading();
            //login successfully save in session
            Session.setItemObject('current_user_data',response.user);
            Session.setItem('token',response.token);
            registerPushNotifiication();
            if(response.user && response.user.type == 'Supplier'){
              $state.go('menu.dashboard');
            }else {
              $state.go('menu.landing');
            }
          }).error(function(error){
            Utils.hideLoading();
            Utils.debug("Error while login facebook" + error);
            Utils.alertPopup('Login Error','Please try later');
          });

        } else {
          Utils.hideLoading();
          facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
        }
      })
    }else{
      Utils.hideLoading();
      Utils.debug('not mobile, unable to get facebookConnectPlugin');
    }

  }

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse) {
      fbLoginError("Cannot find the authResponse");
      return;
    }
    var auth_token = response.authResponse;
    var auth_promise = User.auth($scope.type,'facebook',auth_token);
    auth_promise.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.user);
      Session.setItem('token',response.token);
      registerPushNotifiication();
      if(response.user && response.user.type == 'Supplier'){
        $state.go('menu.dashboard');
      }else {
        $state.go('menu.landing');
      }
    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("Unable to send token to serve: "+ error);
      Utils.alertPopup('Login Error','Please try later');
    });
  };

  // *******************************************
  // This is the fail callback from the login method
  var fbLoginError = function(error) {
    Utils.debug('fbLoginError', error);
    Utils.hideLoading();
  };

  $scope.signIn = function() {
    Utils.showLoading();
    var promise_login = User.signIn($scope.user);
    promise_login.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.user);
      Session.setItem('token',response.token);
      registerPushNotifiication();
      if(! $rootScope.user.active){
        $scope.verify_num_window();
      }
      else if(response.user.type == 'Supplier'){
        $state.go('menu.dashboard');
      }else {
        $state.go('menu.landing');
      }
    }).error(function (error){
      Utils.hideLoading();
      Utils.debug("user login",error);
      if(error){
        Utils.alertPopup('Login Error',error.message);
      }
      else{
        Utils.alertPopup("No internet","Please connect to internet");
      }

    });
  }

  // ********************************************
  // Login With googleplus
  $scope.gPlusLogin = function() {

    Utils.showLoading();
    if(window.plugins.googleplus != undefined){
      window.plugins.googleplus.login(
        {
          'offline': true
        },
        function (user_data) {
          if(user_data.oauthToken){
            var auth_promise = User.auth($scope.type,'google',user_data.oauthToken);
            auth_promise.success(function(response){
              Utils.hideLoading();
              Session.setItemObject('current_user_data',response.user);
              Session.setItem('token',response.token);
              registerPushNotifiication();
              if(response.user && response.user.type == 'Supplier'){
                $state.go('menu.dashboard');
              }else {
                $state.go('menu.landing');
              }
            }).error(function(error){
              Utils.hideLoading();
              Utils.debug("send token error",error);
            });
          }else{
            Utils.debug("Unable to get Gplus token",user_data);
          }
          Utils.hideLoading();
        },
        function (msg) {
          Utils.hideLoading();
        }
      );
    }else{
      Utils.hideLoading();
      Utils.debug('not mobile, unable to get googlePlugin');
    }

  };

  //signup window client
  $scope.signup_window_client = function () {
    $ionicModal.fromTemplateUrl('templates/modals/signupStep1.html', function (modal) {
      $scope.signup_modal = modal;
      $scope.signup_modal.show();
    }, {
      scope: $scope
    });
  };
  //signup window supplier
  $scope.signup_supplier = function () {
    $state.go('chooseCategory');

  };

  //signup window
  $scope.signup_step_2 = function () {
    $scope.user = Session.getItemObject('current_user_data');
    $scope.password = {type:'password',show:false};
    $ionicModal.fromTemplateUrl('templates/modals/signupStep2.html', function (modal) {
      $scope.signup_step2_modal = modal;
      $scope.signup_step2_modal.show();
    }, {
      scope: $scope
    });
  };
  $scope.signUpStep2Form = function(){
    Utils.showLoading();
    var data = $scope.user;
    data.from_mobile = true;
    data.step = 2;
    if($scope.type == 'client'){
      var update_client_promie = Client.update(data);
      update_client_promie.success(function(response){
        Utils.hideLoading();
        $rootScope.user = response.client;
        Session.setItemObject('current_user_data',response.client);
        //save response to user
        $scope.cancel_signup_step2_window();
        registerPushNotifiication();
        $state.go('menu.landing');

      }).error(function(error){
        Utils.hideLoading();
        Utils.alertPopup('Error',error);
        Utils.debug("unable to update",error);
      });
    }

  }
  //
  $scope.create_user = function(){
    Utils.showLoading();
    var data = $scope.user;
    data.from_mobile = true;
    data.step = 1;
    if($scope.type == 'client'){
      var create_client_promie = Client.create(data);
      create_client_promie.success(function(response){
        Utils.hideLoading();
        var user = {};
        if(response.id){
          user.id = response.id;
        }
        else{
          user = response.client;
        }
        Session.setItemObject('current_user_data',user);
        //save response to user
        $scope.cancel_signup_window();
        $scope.verify_num_window();

      }).error(function(error){
        Utils.hideLoading();
        Utils.alertPopup('SignUp Error',error);
        Utils.debug("unable to create",error);
      });

    }
  }
  //verification window
  $scope.verify_num_window = function () {
    var hasSupport = false;
    if(smsInboxPlugin){
      smsInboxPlugin.isSupported ((function(supported) {

        if(supported){
          Utils.debug(supported);

          smsInboxPlugin.startReception (function(message) {
            var thenum = message.match(/\d+/)[0];
            $scope.user.code = thenum;
            $scope.verfiy_user_num();
          }, function(err) {
            Utils.debug(err);
            Utils.alertPopup("Error while reading message, please enter manually");
          });
        }
        else{
          Utils.debug("SMS not supported");
        }
        $ionicModal.fromTemplateUrl('templates/modals/verify_num.html', function (modal) {
          $scope.verification = modal;
          $scope.verification.show();
        }, {
          scope: $scope
        });
      }), function() {
        Utils.debug("Error while checking the SMS support");
      });
    }else{
      $ionicModal.fromTemplateUrl('templates/modals/verify_num.html', function (modal) {
        $scope.verification = modal;
        $scope.verification.show();
      }, {
        scope: $scope
      });
    }

  };

  $scope.cancel_signup_window = function(){
    $scope.signup_modal.remove();
  }
  $scope.cancel_signup_step2_window = function(){
    $scope.signup_step2_modal.remove();
  }

  $scope.cancel_verify_num = function(){
    smsInboxPlugin.stopReception (function() {
      Utils.debug("SMS reception Correctly stopped");
    }, function() {
      Utils.debug("Error while stopping the SMS receiver");
    });
    $scope.verification.remove();
  }

  $scope.verfiy_user_num = function(){
    var user = Session.getItemObject('current_user_data');
    var id = user.id;
    Utils.showLoading();
    var data = {};
    data.code = $scope.user.code;
    var verify_user_promise = User.verifyCode(id,data);
    verify_user_promise.success(function(response){
      Utils.hideLoading();
      $scope.cancel_verify_num();
      Session.setItemObject('current_user_data',response.user);
      Session.setItemObject('token',response.token);
      registerPushNotifiication();
      if(response.user.profile_completed){
        if(response.user && response.user.type == 'Supplier'){
          $state.go('menu.dashboard');
        }else {
          $state.go('menu.landing');
        }
      }else{
        //save response to user
        $scope.signup_step_2();

      }

    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("num not verified",error);
      Utils.alertPopup("Error",error.message);
    });
  }
  // ********************************************
  // Resend SMS
  $scope.resend_code = function(){

    var user = Session.getItemObject('current_user_data');
    var id = user.id;
    Utils.showLoading();
    var resend_code_promise = User.resendCode(id);
    resend_code_promise.success(function(response){
      Utils.hideLoading();
    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("unable to send",error);
      Utils.alertPopup("Error",error.message);
    });
  }
  // Change phone number
  $scope.change_phone_num = function(){
    $scope.user.code="";
    $scope.user.phone="";
    $scope.cancel_verify_num();
    $scope.signup_window_client();

  }
  var registerPushNotifiication = function(){
    if(Session.getItem('device_id')){
      var pn_data={'pns_id':Session.getItem('device_id')};
      if(Utils.is_android()){
        pn_data.type = 'android'
      }else if(Utils.is_ios()){
        pn_data.type = 'ios'
      }
      User.sendPushToken(pn_data);
    }
  }
  init();
}
])
