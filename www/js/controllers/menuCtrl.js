application.controller('menuCtrl',['$scope', '$rootScope', '$state','$ionicHistory','$ionicPopover','$ionicSideMenuDelegate','Session','stringConstants','Supplier','Utils','config','User', function($scope, $rootScope, $state,$ionicHistory,$ionicPopover,$ionicSideMenuDelegate,Session,stringConstants,Supplier,Utils,config,User) {
  $scope.prefix = config.SERVER_URL;
  var init = function(){
    $scope.notification = {inboxCount : 0};
    if($rootScope.user && $rootScope.user.type){
      var subscribtion_key = $rootScope.user.type.toLowerCase() + '_'+ $rootScope.user.id;

      var pusher = new Pusher(config.PUSHER_TOKEN, {
        cluster: 'eu'
      });
      pusher.connection.bind('connected', function() {
        Utils.debug("pusher connected");
      });
      var channel = pusher.subscribe(subscribtion_key);
      channel.bind('new_notification', function(item) {
        $scope.updated_offer_id = item.metadata.offer_id;
        $scope.notification.inboxCount++;
        $scope.$broadcast ("new_update");
      });
      $scope.setImage($rootScope.user);
    }

    // /* PUSHER NOTIFICATIONS */
    // pusher.subscribe(subscribtion_key, 'new_notification', function (item) {
    // });
    $ionicPopover.fromTemplateUrl('templates/popover/selectStatusPopover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.statusPopover = popover;
    });
  }
  $scope.logout = function(){
    Utils.showLoading();
    var logout_promise = User.signOut();
    logout_promise.success(function(response){
      Utils.hideLoading();
      if($rootScope.user.type != undefined){
        var type = $rootScope.user.type.toLowerCase();
        Session.clearSession();
        $state.go('login',{type:type});
      }else{
        Session.clearSession();
        $state.go('login',{type:'client'});
      }
    })
    .error(function(err){
      Utils.hideLoading();
      Session.clearSession();
      $state.go('login',{type:'client'});
    });
  }
  $scope.refer_friend = function(){
    if(window.plugins != undefined){
      window.plugins.socialsharing.share(stringConstants.REFER_FRIEND_TEXT,stringConstants.REFER_FRIEND_SUBJECT);
    }
  }
  $scope.refer_supplier = function(){
    if(window.plugins != undefined){
      window.plugins.socialsharing.share(stringConstants.REFER_SUPPLIER_TEXT,stringConstants.REFER_SUPPLIER_SUBJECT);
    }
  }
  $rootScope.go = function (state,params) {
    $state.go(state,params);
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
  };
  $rootScope.goWithBackOnNext = function (state,params) {
    if(params){
      $state.go(state,params);
    }else{
      $state.go(state);
    }

  };
  $scope.removeNotificationInbox = function (state) {
    $scope.notification.inboxCount = 0;
    $state.go(state);
  };
  $scope.putStatus = function(status){
    var data = {};
    data.id = $rootScope.user.id;
    data.status = status;
    Utils.showLoading();
    var supplier_status_promise = Supplier.update(data);
    supplier_status_promise.success(function(response){
      Session.setItemObject('current_user_data',response.supplier);
      $scope.statusPopover.hide();
      if( $ionicSideMenuDelegate.isOpen() ){
        $ionicSideMenuDelegate.toggleLeft();
      }
      Utils.hideLoading();
    }).error(function(err){
      $scope.statusPopover.hide();
      if( $ionicSideMenuDelegate.isOpen() ){
        $ionicSideMenuDelegate.toggleLeft();
      }
      Utils.hideLoading();
      Utils.alertPopup('Error','Try later');
    });
  }
  $scope.setImage = function(user){
    if(user.image && user.image.url ){
      user.profilePic =   config.SERVER_URL + user.image.url;
    }
  };
  $scope.offerDetails = function(job){
    if(job.my_bid){
      $state.go('menu.proposalDetail',{id:job.id});
    }else {
      $state.go('menu.jobDetail',{category:job.category_slug,id:job.id});
    }
  }

  init();
}
]);
