application.controller('myAddressesCtrl',['$scope','$rootScope','$ionicModal','$ionicPopover','stringConstants','Utils','Client','Session', function ($scope,$rootScope,$ionicModal,$ionicPopover,stringConstants,Utils,Client,Session) {

  var init = function(){
    $scope.navTitle = "My Addresses";
    $scope.myAddresses = $rootScope.user.addresses;
    $scope.countries=stringConstants.COUNTRIES;
    $scope.cities = stringConstants.CITIES;
    $scope.areas = stringConstants.AREAS;
    $ionicPopover.fromTemplateUrl('templates/popover/addressActionPopover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });
  }
  $scope.areaString = function(address){
    if(stringConstants.AREAS){
      try{
        return stringConstants.AREAS[address.city][address.area];
      }catch(err){
        return "Not available"
      }
    }
  }
  $scope.addAddressModal = function(){
    $scope.address = {};
    $ionicModal.fromTemplateUrl('templates/modals/addAddress.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  }
  $scope.myLocation  = function(){
    //doing this stupidity beacuse in database we are saving our own short codes
    var reverse_city_maping = {
      "Dubai":"db",
      "Sharjah":'sh',
      "Ras Al-Khaimah":"rk",
      "Ajman":"aj"
    };
    //doing this stupidity beacuse in database we are saving our own short codes
    var reverse_area_maping = {
      "Dubai Silicon Oasis":"dso",
      "Media City":"mc",
      "Jebal Ali":"ja",
      "Al Karama":"km"
    };
    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
      if(enabled){
        var options= {
          template: '<ion-spinner icon="lines"></ion-spinner>',
          hideOnStateChange:true,
          noBackdrop:true
        }

        Utils.showLoadingWithOptions(options);
        var geocoder = new google.maps.Geocoder();
        navigator.geolocation.getCurrentPosition(function(pos) {
          var setLocalities = function(results){
            angular.forEach(results, function(obj){
              angular.forEach(obj.types,function(type){
                if(type=="country"){
                  $scope.address.country = obj.short_name.toLowerCase();
                }else if(type=="locality"){
                  if(reverse_city_maping[obj.short_name])
                  $scope.address.city = reverse_city_maping[obj.short_name];
                }else if(type=="sublocality"){
                  $scope.address.area = reverse_area_maping[obj.short_name];
                }
              })
            });
          };
          var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
          geocoder.geocode({
            'latLng': latlng
          }, function(results, status) {
            Utils.hideLoading();
            if (status == google.maps.GeocoderStatus.OK && results.length) {
              results = results[0].address_components;
              setLocalities(results);
            }
          });
        }, function(error) {
          Utils.hideLoading();
          Utils.debug('Unable to get location: ' + error.message);
        });
      }else{
        confirm_popup_promise = Utils.confirmPopup("","GPS disables, Please turn ON?","OK");
        confirm_popup_promise.then(function(res) {
          if(res) {
            cordova.plugins.diagnostic.switchToLocationSettings();
          }
        });
      }
    }, function(error){
      console.error("The following error occurred: "+error);
    });

  }

  $scope.cancel_modal = function(){
    $scope.modal.hide();
  }
  $scope.addAddress = function(address){

    var client = {
      id:$rootScope.user.id,
      addresses_attributes:[address]
    };
    Utils.showLoading();
    var add_address_promise = Client.update(client);
    add_address_promise
    .success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.client);
      $rootScope.user = response.client;
      $scope.myAddresses = $rootScope.user.addresses;
      $scope.modal.hide();
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.alertPopup("Network Error","Unable to update Client");
    });
  }
  $scope.openPopOver = function($event,index){
    $scope.popover.show($event);
    $scope.index = index;
  }
  $scope.edit = function(index){
    $scope.popover.hide();
    $scope.address =   $scope.myAddresses[$scope.index];
    $ionicModal.fromTemplateUrl('templates/modals/addAddress.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  }
  $scope.delete = function(){
    $scope.popover.hide();
    var address = $scope.myAddresses[$scope.index];
    var client = {
      id:$rootScope.user.id,
      addresses_attributes:[{id:address.id,_destroy:true}]
    };
    confirm_popup_promise = Utils.confirmPopup("","Are you sure to delete this address?","CONFIRM");
    confirm_popup_promise.then(function(res) {
      if(res) {
        Utils.showLoading();
        var promise_delete_address = Client.update(client);
        promise_delete_address.success(function(response){
          Utils.hideLoading();
          Session.setItemObject('current_user_data',response.client);
          $rootScope.user = response.client;
          $scope.myAddresses = $rootScope.user.addresses;
        }).error(function (error){
          Utils.hideLoading();
          Utils.debug("Delete address error",error)
          Utils.alertPopup('Unable Delete address',error.message);
        });

      }
    });
  }
  init();
}]);
