application.controller('myJobsCtrl',['$scope','$rootScope','$state', '$ionicModal','$ionicSideMenuDelegate','$ionicScrollDelegate','Offer','Utils','Supplier', function ($scope,$rootScope,$state, $ionicModal, $ionicSideMenuDelegate,$ionicScrollDelegate,Offer,Utils,Supplier) {
  var init = function(){
    $scope.navTitle = "My Jobs";
    $scope.option={tab:0};
    $scope.locations = {1: 'Al Bahriah',2: 'Al Wajeha',3:'Burdubai',
    4: 'Deira',5: 'Dubai Land',6: 'Jabel Ali',
    7: 'Jumeira',8: 'Madinat',9: 'Mushrif',10: 'Ras Al Khor'
  };
  $scope.tags =  ['ongoing_jobs','saved_jobs','completed_jobs','proposed_jobs'];
  $scope.numdays = [{id: 0, text: 'Urgent'},
  {id: 1, text: '1 Day'},
  {id: 2, text: '2 Days'},
  {id: 3, text: '3 Days'},
  {id: 4, text: '4 Days'},
  {id: 5, text: '5 Days'},
  {id: 6, text: '6 Days'},
  {id: 7, text: '7 Days'}];
  $scope.current_page = 1;
  Utils.showLoading();
  var jobs_list_promise = Supplier.myJobs($rootScope.user.id,$scope.tags[0]);
  jobs_list_promise.success(function(response){
    Utils.hideLoading();
    $scope.jobsList = response.jobs;
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
  });
}
$scope.toggleSideMenu = function() {
  $ionicSideMenuDelegate.toggleLeft();
};
$scope.offerDetails = function(job){
  if(job.my_bid){
    $state.go('menu.proposalDetail',{id:job.id});
  }else {
    $state.go('menu.jobDetail',{'category':job.category_slug,'id':job.id});
  }
}

$scope.$watch('search_string', function () {
  $ionicScrollDelegate.scrollTop();
});

$scope.resetFilter = function(search_qry){
  $scope.search_qry={};
  $scope.slider.minValue = 0;
  $scope.slider.maxValue = 5000;
};
$scope.newTabOffers = function(index){
  console.log(index);

  $scope.option.tab = index;
  $scope.jobsList = [];
  Utils.showLoading();
  var jobs_list_promise ;
  jobs_list_promise = Supplier.myJobs($rootScope.user.id,$scope.tags[index]);
  jobs_list_promise.success(function(response){
    Utils.hideLoading();
    console.log(response);
    $scope.jobsList = response.jobs;
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
  });
}
$scope.saveJob = function(index){
  var id = $scope.jobsList[index].id;
  Utils.showLoading();
  var save_job_promise = Offer.saveJob(id);
  save_job_promise
  .success(function(response){
    Utils.hideLoading();
    $scope.jobsList[index] = response.offer;
  })
  .error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
    Utils.alertPopup("Error","Network Error please try later");
  });
};
init();
}]);
