application.controller('myOfferCtrl',['$scope','$state', '$ionicModal','$ionicScrollDelegate','$ionicPopover','Offer','Utils', function ($scope,$state, $ionicModal,$ionicScrollDelegate,$ionicPopover,Offer,Utils) {

  var init = function(){
    $scope.option={};
    $scope.navTitle = 'My Offers';
    $scope.locations = {1: 'Al Bahriah',2: 'Al Wajeha',3:'Burdubai',
    4: 'Deira',5: 'Dubai Land',6: 'Jabel Ali',
    7: 'Jumeira',8: 'Madinat',9: 'Mushrif',10: 'Ras Al Khor'
  };
  $scope.current_page = 1;
  $scope.search_qry = {};
  Utils.showLoading();
  var jobs_list_promise = Offer.offersList(1);
  jobs_list_promise
  .success(function(response){
    Utils.hideLoading();
    $scope.offerList = response.offers;
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug('Error while fetching offer',err);
  });

}
$scope.loadMore = function(page_num){
  $scope.current_page = page_num;
  Utils.showLoading();
  var jobs_list_promise = Offer.offersList(page_num);
  jobs_list_promise
  .success(function(response){
    Utils.hideLoading();
    $scope.offerList = $scope.offerList.concat(response.offers);
    $scope.total_pages = response.total_pages;
  }).error(function(err){
    Utils.hideLoading();
  });

};
$scope.doRefresh = function() {

  $scope.current_page = 1;
  var jobs_list_promise = Offer.offersList(1);
  jobs_list_promise
  .success(function(response){
    $scope.offerList = response.offers;
    $scope.total_pages = response.total_pages;
  })
  .finally(function() {
    // Stop the ion-refresher from spinning
    $scope.$broadcast('scroll.refreshComplete');
  });
};

$scope.close = function () {
  $scope.modal.remove();
};
$scope.closeFilter = function(){
  $scope.filterModal.remove();
}
$scope.$watch('search_string', function () {
  $ionicScrollDelegate.scrollTop();
});
$scope.openPopover = function($event,index) {
   $scope.popover = null;
   $scope.index = index;
   $scope.current_offer = $scope.offerList[$scope.index];
   $ionicPopover.fromTemplateUrl('templates/popover/offerResponsePopover.html', {
     scope: $scope,
   }).then(function(popover) {
     $scope.popover = popover;
     $scope.popover.show($event);
   });

 };
 $scope.offerDetailPopover = function($event) {
    $ionicPopover.fromTemplateUrl('templates/popover/offerDetailPopover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
      $scope.popover.show($event);
    });

  };
 $scope.openModalOnItemClick = function(index){
   $scope.index = index;
   $scope.openProposalModal();
 }
 $scope.openPopOverOnModal = function(){
   $scope.index = index;
   $ionicPopover.fromTemplateUrl('templates/popover/offerResponsePopover.html', {
     scope: $scope,
   }).then(function(popover) {
     $scope.popOverOnModal = popover;
     $scope.popOverOnModal.show($event);
   });
 };
$scope.openProposalModal = function(){
  $scope.cancel_modal();
  $scope.current_offer = $scope.offerList[$scope.index];
  $ionicModal.fromTemplateUrl('templates/modals/proposalList.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
    $scope.modal.show();
  });
};
$scope.openProposalFilter = function(){
  $scope.current_offer = $scope.offerList[$scope.index];
  $ionicModal.fromTemplateUrl('templates/modals/proposalFilter.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.filterModal = modal;
    $scope.filterModal.show();
  });
};

$scope.openOfferDetailModal = function(){
  $scope.cancel_modal();
  $scope.current_offer = $scope.offerList[$scope.index];
  $ionicModal.fromTemplateUrl('templates/modals/offerDetail.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
    $scope.modal.show();
  });
};
$scope.showPopOverOfferDetail = function () {

};
$scope.cancel_modal = function () {
  if($scope.modal){
    $scope.modal.remove();
  }
  if($scope.popover){
    $scope.popover.remove();

  }
  if($scope.popOverOnModal){
    $scope.popOverOnModal.remove();
  }
};
$scope.proposalDetail = function(id){
  $scope.cancel_modal();
  $state.go('menu.proposalDetail',{id:id});
}
$scope.invite = function(offer){
  $scope.cancel_modal();
  $state.go('menu.viewSupplier',{category:offer.category_slug,id: offer.id});
}
$scope.repostOffer = function(offer){
  $scope.cancel_modal();
  $state.go('menu.offerMethod',{category:offer.category_slug,repost: true});
  Offer.repostOffer = offer;
  Offer.category_id = offer.category_id.$oid;
}

$scope.resetFilter = function(search_qry){
  $scope.search_qry={};
};

$scope.updateBids = function(){
  Utils.showLoading();
  var bids_promise = Offer.offerBids(1,$scope.current_offer.id,$scope.search_qry);
  bids_promise.success(function(response){
    Utils.hideLoading();
    $scope.current_offer.bids = response.bids;
    $scope.closeFilter();
  }).error(function(err){
    Utils.hideLoading();
    Utils.debug(err);
  });
}
$scope.newTabMyOffer = function(index){
  $scope.option.tab = index;
  if(index==1){
    Utils.showLoading();
    // $scope.current_offer
    var invited_supplier_promise = Offer.invitedSuppliers($scope.current_offer.id);
    invited_supplier_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.invited_suppliers = response.suppliers;
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
      console.log(err);
    });

  }
}
init();
}]);
