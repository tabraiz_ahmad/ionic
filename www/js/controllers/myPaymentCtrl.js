application.controller('myPaymentCtrl',['$scope','$rootScope','$ionicModal','Utils','Offer','Transaction','config', function ($scope,$rootScope,$ionicModal,Utils,Offer,Transaction,config) {

  var fromDatePickerCallback = function (val) {
    if (typeof(val) != 'undefined') {
      $scope.search_qry.from_date = new Date(val);
    }
  };
  var toDatePickerCallback = function (val) {
    if (typeof(val) != 'undefined') {
      $scope.search_qry.to_date = new Date(val);
    }
  };

  var init = function(){
    $scope.navTitle = "Payment History";
    $scope.search_qry = {};
    $scope.isSupplier = ( $rootScope.user.type == 'Supplier');
    $scope.getTransactions();

    Utils.showLoading();
    var sub_categories_promise = Offer.allSubCategories();
    sub_categories_promise.success(function(response){
      $scope.subCategories = response;

      Utils.hideLoading();
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });
    $scope.fromDateObject = {
      inputDate: new Date(),  //Optional
      templateType: 'popup', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(2010,1,1),
      to: new Date(),
      dateFormat: 'dd-MM-yyyy',
      closeOnSelect: true,
      callback: function (val) {
        fromDatePickerCallback(val);
      }
    };
    $scope.toDateObject = {
      inputDate: new Date(),  //Optional
      templateType: 'popup', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: $scope.search_qry.from_date,
      to: new Date(),
      dateFormat: 'dd-MM-yyyy',
      closeOnSelect: true,
      callback: function (val) {
        toDatePickerCallback(val);
      }
    };
  }
  $scope.paymentFilter = function() {
    $ionicModal.fromTemplateUrl('templates/modals/paymentFilter.html', function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    }, {
      scope: $scope
    });
  }
  $scope.close = function () {
    if($scope.modal)
    $scope.modal.remove();
  };
  $scope.resetFilter = function(search_qry){
    $scope.search_qry={};
  };
  $scope.getTransactions = function(){
    Utils.showLoading();
    var transaction_promise = Transaction.find($scope.search_qry);
    transaction_promise.success(function(response){
      Utils.hideLoading();
      $scope.transactions = response.transactions;
      $scope.close();
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    });
  };
  $scope.download = function(transaction_id){
    if(window.cordova && window.cordova.InAppBrowser){
      var url = 'https://docs.google.com/viewer?url=' + encodeURIComponent(config.API_BASE_PATH + '/transactions/'+ transaction_id + '/download');
      var inAppBrowserRef = cordova.InAppBrowser.open(url, '_blank', 'location=no,hidden=yes');
      inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack);
      inAppBrowserRef.addEventListener('loadstart', loadStartCallBack);
      inAppBrowserRef.addEventListener('loadstop', loadStopCallBack);
      inAppBrowserRef.addEventListener('exit', iabClose);
      function loadStartCallBack(params){
        Utils.showLoading();
      }
      function loadStopCallBack(params){
        Utils.hideLoading();
        inAppBrowserRef.show();
      }
      function loadErrorCallBack(params) {
        console.log(params);

      }
      function iabClose(event) {
        inAppBrowserRef.removeEventListener('loadstart', loadStartCallBack);
        inAppBrowserRef.removeEventListener('loadstop', loadStopCallBack);
        inAppBrowserRef.removeEventListener('loaderror', loadErrorCallBack);
        inAppBrowserRef.removeEventListener('exit', iabClose);
      }
    }else{
      var browserRef = window.open(config.API_BASE_PATH + '/transactions/'+ transaction_id + '/download', '_blank');
    }


  }
  init();
}]);
