application.controller('offerCreatedSuccessCtrl',['$scope','$rootScope','$state','Offer','Utils', function ($scope,$rootScope,$state, Offer, Utils) {

  var init = function(){
    $scope.navTitle = "Post Published";
    $scope.Offer = Offer.createdOffer;
  }

  $scope.deleteOffer = function(){

    var confirm_popup_promise = Utils.confirmPopup("Delete Offer","Are you sure you want to delete this offer?","DELETE");
    confirm_popup_promise.then(function(res) {
      if(res){
        Utils.showLoading();
        var offer_delete_promise = Offer.delete($rootScope.user.id,$scope.Offer.id);
        offer_delete_promise.success(function(response){
          Utils.hideLoading();
          Utils.alertPopup("Offer Deleted",response.message);
          $scope.go('menu.landing');
        }).error(function(err){
          Utils.alertPopup("Unable to delete offer",err);
        });
      }
    });
  };
  $scope.editOffer = function(){
    // /offer/house-cleaning/bid/offer_id/supplier_id
    $state.go('menu.offer',{'category':$scope.Offer.category_slug,'method':$scope.Offer.bid_type,'id':$scope.Offer.id,'supplier':$scope.Offer.supplier_id[0].$oid});
  }
  init();
}
]);
