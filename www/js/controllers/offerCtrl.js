application.controller('offerCtrl',['$scope','$rootScope','$state','Session','Utils','$stateParams','$ionicModal','$ionicHistory','$ionicScrollDelegate','subcategories','Offer','User','Supplier','config','stringConstants','Client',function($scope, $rootScope, $state, Session, Utils, $stateParams,$ionicModal,$ionicHistory,$ionicScrollDelegate,subcategories,Offer, User, Supplier, config,stringConstants,Client){
	$scope.method = $stateParams.method;
	$scope.category = $stateParams.category;
	$scope.prefix = config.SERVER_URL;
	$scope.includings_string  = "";
	var init = function(){
		$scope.options = {show: true};
		$scope.id = $stateParams.id;
		$scope.repost = $stateParams.repost;
		var supplier_id = $stateParams.supplier;


		if(supplier_id){
			findSupplier(supplier_id);
		}
		if($scope.id){
			Utils.showLoading();
			var find_offer_promise = Offer.findOffer($scope.id);
			find_offer_promise.success(function(response){
				Utils.hideLoading();
				$scope.offer = response.offer;
				if( !$scope.supplier){
					if($scope.offer.supplier_id && !$scope.repost){
						findSupplier($scope.offer.supplier_id.$oid)
					}
				}
				if($scope.offer.category_slug == "laundry"){
					$scope.navTitle = 'Laundry';
					$scope.laundryIncludings = {wash:"",iron:"",dryclean:""};
					laundri_string($scope.offer.detail,'wash');
					laundri_string($scope.offer.detail,'iron');
					laundri_string($scope.offer.detail,'dryclean');
				}else{
					makeIncludingString();
				}
				if($scope.offer.has_budget === false){
					$scope.offer.has_budget = false;
				}else{
					$scope.offer.has_budget = true;
				}
				if($scope.repost){
					if($scope.offer.date_time)
					delete $scope.offer.date_time;
					if($scope.offer.return_date_time)
					delete $scope.offer.return_date_time;
					if($scope.offer.id)
					delete $scope.offer.id;
					if($scope.offer.done_in)
					delete $scope.offer.done_in;
				}
			}).error(function(err){
				Utils.hideLoading();
				Utils.debug(err);
			})
		}else{
			$scope.offer =   {has_budget: true};
		}
		var datePickerCallback = function (val) {
			if (typeof(val) != 'undefined') {
				$scope.specificDate = new Date(val);
			}
		};
		$scope.dateObject = {
			inputDate: new Date(),  //Optional
			templateType: 'popup', //Optional
			modalHeaderColor: 'bar-positive', //Optional
			modalFooterColor: 'bar-positive', //Optional
			from: new Date(),
			to: new Date(2050,1,1),
			dateFormat: 'dd-MM-yyyy',
			closeOnSelect: true,
			callback: function (val) {
				datePickerCallback(val);
			}
		};
		$scope.timeObject = {
			inputEpochTime: ((new Date()).getHours() * 60 * 60), //Optional
			step: 15, //Optional
			format: 12, //Optional
			titleLabel: 'Start Time (12-hour format)', //Optional
			setLabel: 'Set', //Optional
			closeLabel: 'Close', //Optional
			setButtonType: 'button-positive', //Optional
			closeButtonType: 'button-stable', //Optional
			callback: function (val) {    //Mandatory
				timePickerCallback(val);
			}
		};
		function timePickerCallback(val) {
			if (typeof (val) != 'undefined') {
				$scope.specificTime = val * 1000;
				$scope.str_time = Utils.epochParser(val,'time')
			}
		}


		if($scope.offer == undefined){
			$scope.specificDate = null;
			$scope.specificTime =null;
		}
		if($scope.category == subcategories.HOUSE_CLEANING){

			init_house_cleaning();
		}else if($scope.category == subcategories.RENT_A_CAR){

			init_rent_car();
		}else if($scope.category == subcategories.PEST_CONTROL){

			init_pest_control();
		}else if($scope.category == subcategories.AC){
			init_ac_service();
		}else if($scope.category == subcategories.PLUMBING){
			init_plumbing_service();
		}else if($scope.category == subcategories.LAUNDRY){
			init_laundry_service();
		}
		$scope.showAdd = {'add':false}
		$scope.countries=stringConstants.COUNTRIES;
    $scope.cities = stringConstants.CITIES;
    $scope.areas = stringConstants.AREAS;
	}
	function findSupplier(supplier_id) {
		$scope.supplier = {};
		$scope.supplier.id = supplier_id;
		$scope.hideBudget = true;
		Utils.showLoading();
		var find_supplier_promise = Supplier.find($scope.supplier);
		find_supplier_promise.success(function(response){
			Utils.hideLoading();
			$scope.supplier = response.supplier;
		}).error(function(err){
			Utils.hideLoading();
			Utils.debug(err);
		})
	}
	var init_house_cleaning = function(){
		$scope.navTitle = 'Cleaning';
		$scope.dateObject.titleLabel = 'Offer Date';
		$scope.includings = [{value: 'Dusting, Mopping, and Vaccuming all rooms',key:"dusting"},
		{value: 'Trash removal to your outdoor cans', key: "trash_removal"},
		{value: 'Kitchen counters, floor and stove cleaning', key:"kitchen_counter"},
		{value: 'Bathroom vanity, tub and toilet cleaning',key:"bathroom_vanity"},
		{value: 'Window washing, wall washing and interior cleaning',key:"window_washing"}];
	};
	var init_rent_car = function(){
		$scope.navTitle = 'Rent a Car';
		$scope.dateObject.titleLabel = 'Pick Up date';
		$scope.includings = [{value: 'ISOFIX',key:"isofix"},
		{value: 'Smoking Allowed', key: "smoking"},
		{value: 'GPS Navigation System', key:"gps"},
		{value: 'With Driver',key:"driver"},
		{value: 'Baby Saftey Seats',key:"baby_safety"},
		{value: 'Airport meet and Greet',key:"airport_meet"}];

		$scope.models = {
			'Audi' : {"Audi Q5":"Audi Q5","Audi Q7":"Audi Q7","Audi RS7":"Audi RS7","Audi Q3":"Audi Q3","Audi S7":"Audi S7"},
			'BMW': {"BMW i8":"BMW i8","BMW M6":"BMW M6","BMW X5":"BMW X5", "BMW Z4":"BMW Z4"},
			'Lamborghini':{"Huracan":"Huracan","Aventador":"Aventador","Asterion":"Asterion"},
			'Volkswagen':{"Passat":"Passat","Beetle":"Beetle","Golf":"Golf","Touareq":"Touareq","Tiguan":"Tiguan"},
			'Toyota':{"Camry":"Camry","Corolla":"Corolla","Rav4":"Rav4","Land Cruiser":"Land Cruiser","Yaris":"Yaris"}
		};

	};
	var init_pest_control = function(){
		$scope.navTitle = 'Pest Control';
		$scope.services = [{value: 'Termite',key:"termite"},
		{value: 'Mouse', key: "mouse"},
		{value: 'Rat', key: "rat"},
		{value: 'Rodent', key: "rodent"},
		{value: 'Bed bug', key: "bed_bug"},
		{value: 'Bird', key: "bird"},
		{value: 'Fly', key:"fly"},
		{value: 'Ant', key:"ant"},
		{value: 'Flea', key:"flea"},
		{value: 'Tick', key:"tick"},
		{value: 'Mosquito', key:"mosquito"},
		{value: 'Cockroach', key:"cockroach"},
		{value: 'Scorpion', key:"scorpion"},
		{value: 'Wasp', key:"wasp"},
		{value: 'Bee', key:"bee"},
		{value: 'Stink bug', key:"stink_bug"},
		{value: 'Lizard', key:"lizard"},
		{value: 'Snake', key:"snake"},
		{value: 'Other', key:"Other"}
	];
}
var init_ac_service = function(){
	$scope.navTitle = 'AC';
	// $scope.pest = [];
	$scope.services = [{value: 'Wet Servicing',key:"wet_service"},
	{value: 'Dry Servicing',key:"dry_service"},
	{value: 'Gas Refilling',key:"gas_refil"},
	{value: 'Installation',key:"installation"},
	{value: 'Other', key:"other"}
];
}
var init_plumbing_service = function(){
	$scope.navTitle = 'PLUMBING';

	// $scope.pest = [];
	$scope.services = [{value: 'Pips Repairing',key:"pipe_repairing"},
	{value: 'Water Pumps',key:"water_pump"},
	{value: 'Water Leakage',key:"water_leakage"},
	{value: 'Drain Blockage',key:"drain_blockage"},
	{value: 'New Installation', key:"new_installation"},
	{value: 'Other', key:"other"}
];
}
var init_laundry_service = function() {
	$scope.navTitle = 'Laundry';
	$scope.laundryIncludings = {};
	$scope.mens_wear = {shirt:"Shirt",suit_2pc:"SUIT 2PC",suit_3pc:"SUIT 3PC",silkshirt:"Silk Shirt",tie:"TIE",overcoat:"OVER COAT",kandurah:"Kandurah",ghutra:"ghutra",wizar_sirwal:"Wizar Sirwal",cap:"Cap",kurta:"Kurta",handkerchief:"Handkerchief",lungi:"Lungi"};
	$scope.ladies_wear = {skirt_pleated:"Skirt Pleated",skirt:"Skirt",blouse:"Blouse",casual_dress:"Casual Dress",saree_abaya:"Saree Abaya",scarf:"Scraf",night_gown:"Night Gown",formal_dress:"Formal Dress",wedding_dress:"Wedding Dress",salwar_khamis_3pc:"Salwar Khamis 3PC",ladies_suit:"Ladies Suit"};
	$scope.other_wear = {sports_pants:"Sports Pants",pants:"Pants",t_shirt:"T-shirt",sweater_pullover:"Sweater Pullover",sports_suit:"Sports Suit",shorts:"Shorts",jacket:"Jacket",underwear:"Underwear",socks_stockings:"Socks Stockings",pyjama_double:"Pyjama Double",bathrobe:"Bathrobe",towel_small:"Towel Small",towel_big:"Towel Big",pillow_case:"Pillow Case",table_cloth_big:"Table Cloth Big",
	bedsheet:"Bedsheet",duvet_comforter:"Duvet Comforter",duvet_cover:"Duvet Cover",cushion_cover_big:"Cushion Cover Big",cushion_cover_small:"Cushion Cover Small",overall:"Overall",apron:"Apron",baby_bottom:"Baby Bottom",baby_top:"Baby Top",shoe_shine:"Shoe Shine"};
}
$scope.cancel_modal = function(){
	$scope.modal.remove();
}
$scope.showDateTimeModal = function(key){
	$scope.key = key;
	$ionicModal.fromTemplateUrl('templates/modals/dateTimeModal.html', function (modal) {
		$scope.modal = modal;
		$scope.modal.show();
	}, {
		scope: $scope
	});
}
$scope.clickIncludings = function(item){
	item.selected = !item.selected;
	//$scope.offer.detail[item.key] = item.selected;
}
$scope.addressModal = function(offer){
	$scope.offer = offer;
	if($rootScope.user && $rootScope.user.id){
		$scope.myAddresses = $rootScope.user.addresses;
		$ionicModal.fromTemplateUrl('templates/modals/selectAddress.html', function (modal) {
			$scope.modal = modal;
			$scope.modal.show();
		}, {
			scope: $scope
		});
	}else{
		login();
	}

}

$scope.areaString = function(address){
	if(stringConstants.AREAS && stringConstants.AREAS[address.city])
	return stringConstants.AREAS[address.city][address.area];
}
$scope.setAddress = function(offer,address){
	if(address.selected){
		offer.address_id = address.id;
		$scope.cancel_modal();
		if(offer.id){
			$scope.updateOffer(offer);
		}else{
			$scope.createOffer(offer);
		}
	}
}
$scope.createOffer = function(offer){
	if($rootScope.user && $rootScope.user.id){
		offer.bid_type = $scope.method;
		if(Offer.category_id){
			offer.category_id = Offer.category_id;
		}else{
			offer.category_id = $scope.category;
		}
		if($scope.method === 'direct'){
			offer.supplier_id = $scope.supplier.id;
		}
		confirm_popup_promise = Utils.confirmPopup("","Are you sure you want to submit request?","CONFIRM");
		confirm_popup_promise.then(function(res) {
			createTitle();
			if(res) {
				Utils.showLoading();
				var promise_offer_create = Offer.create($rootScope.user.id, offer);
				promise_offer_create.success(function(response){
					Utils.hideLoading();
					$scope.offer={};
					$ionicHistory.clearCache();
					Offer.createdOffer = response.offer;
					if($scope.laundryIncludings){
						Offer.createdOffer.laundryIncludings = $scope.laundryIncludings;
					}
					$rootScope.go('menu.offerCreatedSuccess');
				}).error(function (error){
					Utils.hideLoading();
					Utils.debug("offer create error",error)
					Utils.alertPopup('Unable to Create Offer',error.message);
				});

			}
		});
	}else{
		login();
	}
}
$scope.updateOffer = function(offer){
	confirm_popup_promise = Utils.confirmPopup("","Are you sure you want to submit request?"),"CONFIRM";
	confirm_popup_promise.then(function(res) {
		createTitle();
		if(res) {
			Utils.showLoading();
			offer.category_id = offer.category_id.$oid;
			var promise_offer_update = Offer.update($rootScope.user.id, offer);
			promise_offer_update.success(function(response){
				Utils.hideLoading();
				Offer.createdOffer = response.offer;
				if($scope.laundryIncludings){
					Offer.createdOffer.laundryIncludings = $scope.laundryIncludings;
				}
				$rootScope.go('menu.offerCreatedSuccess');

			}).error(function (error){
				Utils.hideLoading();
				Utils.debug("offer create error",error)
				Utils.alertPopup('Unable to Create Offer',error.message);
			});

		}
	});
}
var login = function(){
	confirm_popup_promise = Utils.confirmPopup("Not Login","Please login!","LOGIN");
	confirm_popup_promise.then(function(res) {
		if(res){
			$scope.user={};
			$ionicModal.fromTemplateUrl('templates/modals/login.html', function (modal) {
				$scope.login_modal = modal;
				$scope.login_modal.show();
			}, {
				scope: $scope
			});
		}
	});
}
$scope.logIn = function(){
	Utils.showLoading();
	var promise_login = User.signIn($scope.user);
	$scope.user.type = 'client'
	promise_login.success(function(response){
		Utils.hideLoading();
		Session.setItemObject('current_user_data',response.user);
		Session.setItem('token',response.token);
		$scope.login_modal.hide();
	}).error(function (error){
		Utils.hideLoading();
		Utils.debug("user login",error)
		Utils.alertPopup('Login Error',error.message);

	});
}

$scope.cancel_login = function(){
	$scope.login_modal.hide();
}

$scope.saveDateTime = function(key){
	var day_millis,time_millis;
	if($scope.specificDate){
		day_millis = new Date($scope.specificDate).getTime();
	}else{
		Utils.alertPopup("Select Date","");
		return;
	}
	if($scope.specificTime){
		time_millis = $scope.specificTime;
	}else{
		Utils.alertPopup("Select time","");
		return;
	}
	$scope.offer[key] = new Date(day_millis + time_millis);
	$scope.cancel_modal();
	$scope.specificDate = null;
	$scope.str_time = null;
}
$scope.removeSpecificTime = function(){
	$scope.offer.date_time = null;
}
var createTitle = function(){
	var title = Utils.makeTitle($scope.category)+ ' for ' ;
	if($scope.category == subcategories.HOUSE_CLEANING){
		title += $scope.offer.detail.house_type;
	}else if($scope.category == subcategories.RENT_A_CAR){
		title += $scope.offer.detail.brand + " ";
		title += $scope.offer.detail.car_model;
	}else if($scope.category == subcategories.PEST_CONTROL){
		title += $scope.includings_string.replace(/_/g, " ") + " in " + $scope.offer.detail.property + " property";
	}else if($scope.category == subcategories.AC){
		title = "AC Maintenance for " + $scope.includings_string.replace(/_/g, " ") + " in " + $scope.offer.detail.property + " property";
	}else if($scope.category == subcategories.LAUNDRY){
		var arTitle = [];
		angular.forEach($scope.offer.detail, function(obj, key){
			for (var k in obj){
				if(typeof arTitle[key] === 'undefined') {  // does not exist
					arTitle[key] = obj[k];
				}else{  // does exist
					arTitle[key] += obj[k];
				}
			}
		});

		title += 'Wash('+arTitle['wash']+'), DryClean('+arTitle['dryclean']+'), Iron('+arTitle['iron']+')';
		$scope.offer.title = title;

	}else{
		title += $scope.includings_string;
	}


	$scope.offer.title = title;
};
$scope.getPartial = function(){
	return 'templates/partials/'+$scope.category+'.html';
}
$scope.pestSelectModal = function(){
	$ionicModal.fromTemplateUrl('templates/modals/service_select.html', function (modal) {
		$scope.modal = modal;
		$scope.modal.show();
	}, {
		scope: $scope
	});
}
$scope.acServiceSelectModal = function(){
	$ionicModal.fromTemplateUrl('templates/modals/service_select.html', function (modal) {
		$scope.modal = modal;
		$scope.modal.show();
	}, {
		scope: $scope
	});
}

$scope.laundryModal = function(type){
	$scope.type = type;
	if(type=="wash"){
		$scope.laundry_for = "Washing";
	}else if(type=="dryclean"){
		$scope.laundry_for = "Dry Cleaning";
	}else if(type=="iron"){
		$scope.laundry_for = "Ironing";
	}
	$ionicModal.fromTemplateUrl('templates/modals/laundry_requirement.html', function (modal) {
		$scope.modal = modal;
		$scope.modal.show();
	}, {
		scope: $scope
	});
}

$scope.discard_cancel_modal = function(){
	$scope.modal.hide();
}
$scope.upDateIncludings = function(){
	if(!$scope.offer.detail){
		$scope.offer.detail = {};
	}
	makeIncludingString();
	$scope.modal.hide();
}

function makeIncludingString() {
	var keys = Object.keys($scope.offer.detail);
	$scope.includings_string = keys.filter(function(key) {
		return $scope.offer.detail[key] === true;
	}).join(", ");
}
$scope.minusItem = function(type,name){

	$scope.laundryIncludings[type] = "";
	if(!$scope.offer.detail){
		$scope.offer.detail = {};
	}
	if(!$scope.offer.detail[type]){
		$scope.offer.detail[type] = {};
	}if(!$scope.offer.detail[type][name]){
		$scope.offer.detail[type][name] = 0;
	}
	if($scope.offer.detail[type][name]>0)
	$scope.offer.detail[type][name] = $scope.offer.detail[type][name]-1;
	laundri_string($scope.offer.detail,type);
}
$scope.plusItem = function(type,name,wear_type){
	$scope.laundryIncludings[type] = "";
	if(!$scope.offer.detail){
		$scope.offer.detail = {};
		$scope.offer.detail[wear_type] = true;
	}if(!$scope.offer.detail[type]){
		$scope.offer.detail[type] = {};
	}if(!$scope.offer.detail[type][name]){
		$scope.offer.detail[type][name] = 0;
	}
	$scope.offer.detail[type][name] = $scope.offer.detail[type][name]+1;
	laundri_string($scope.offer.detail,type);
}
var laundri_string = function(l_service,type){
	angular.forEach(l_service[type], function(quantity, name){
		if(quantity > 0){
			if($scope.mens_wear[name]){
				$scope.laundryIncludings[type] = $scope.laundryIncludings[type] + $scope.mens_wear[name] + ' (' + quantity + '),';
			}else if ($scope.ladies_wear[name]){
				$scope.laundryIncludings[type] = $scope.laundryIncludings[type] + $scope.ladies_wear[name] + ' (' + quantity + '),';
			}else if ($scope.other_wear[name]){
				$scope.laundryIncludings[type] = $scope.laundryIncludings[type] + $scope.other_wear[name] + ' (' + quantity + '),';
			}
		}
	});
}
$scope.addNewAddress = function(){
	$scope.showAdd.add = true;
	$ionicScrollDelegate.scrollBottom(true);
}
$scope.addAddress = function(address){

	var client = {
		id:$rootScope.user.id,
		addresses_attributes:[address]
	};
	Utils.showLoading();
	var add_address_promise = Client.update(client);
	add_address_promise
	.success(function(response){
		Utils.hideLoading();
		Session.setItemObject('current_user_data',response.client);
		$rootScope.user = response.client;
		$scope.myAddresses = $rootScope.user.addresses;
		$scope.showAdd.add = false;

	})
	.error(function(err){
		Utils.hideLoading();
		Utils.alertPopup("Network Error","Unable to update Client");
	});
}
$scope.cancelData = function(){
	confirm_popup_promise = Utils.confirmPopup("","If you cancel your changes will not be saved?","CONFIRM");
	confirm_popup_promise.then(function(res) {
		if(res) {
			$scope.offer={};
			$rootScope.go('menu.landing')
		}
	});
}
init();
}
]);
