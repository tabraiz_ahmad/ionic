application.controller('offerMethodCtrl',['$scope','$state','$stateParams','Offer',function($scope,$state,$stateParams,Offer){
	var init = function(){
		$scope.method={};
		$scope.category = $stateParams.category;
		if($stateParams.repost){
			$scope.repost = true;
			if(Offer.repostOffer.bid_type)
				$scope.method[Offer.repostOffer.bid_type] = true;
		}
	}

	$scope.bid = function(){
		$scope.method = {bid:true};
		if($scope.repost){
			$state.go('menu.offer',{category: $scope.category,method:'bid',repost:true,id:Offer.repostOffer.id});
		}else{
			$state.go('menu.offer',{category: $scope.category,method:'bid'});
		}
	}

	$scope.direct = function(){
		$scope.method = {direct:true};
		if($scope.repost){
			$state.go('menu.viewSupplier',{category: $scope.category,repost:true,id:Offer.repostOffer.id});
		}else{
			$state.go('menu.viewSupplier',{category: $scope.category});
		}
	}

	init();
}
])
