application.controller('profileCtrl',['$scope','$rootScope','$state','$ionicFilterBar','Session','config','Search','Client','Supplier','Utils', function ($scope,$rootScope,$state,$ionicFilterBar,Session,config,Search,Client,Supplier,Utils) {

  var init = function(){
    $scope.navTitle = "Profile";
    $scope.setImage($rootScope.user);
    if($rootScope.user.type == 'Client'){
      Utils.showLoading();
      var review_badges = Client.badges();
      review_badges
      .success(function(response){
        Utils.hideLoading();
        $scope.level = response.level;
      })
      .error(function(error){
        Utils.hideLoading();
        Utils.debug('Unable to fetch your achievments',error);
      });
    }
  };
  $scope.setImage = function(user){
    if(user.image && user.image.url){
      user.profilePic =   config.SERVER_URL + user.image.url;
    }
  };
  $scope.jobsList = function(){
    $rootScope.go('menu.jobsList');
  };
  $scope.myDocuments = function(){
      $state.go('menu.supplierDocuments');
  };
  $scope.bankDetails = function(){
      $state.go('menu.supplierBankDetails');
  };
  $scope.addresses = function(){
      $state.go('menu.supplierServiceArea');
  };
  $scope.showFilterBar = function(){
		$scope.searchOpen = {value:true};
		$ionicFilterBar.show({
			update: function (filteredItems,searchText) {
				if(searchText){
					var promise_search_text = Search.searchList(searchText);
					promise_search_text
					.success(function(response){
						$scope.searchResult = response;
					})
					.error(function(error){
						Utils.debug('Error while searching',error);
					});
				}
			},
			cancel: function(){
				$scope.searchOpen = {value:false};
				$scope.searchResult = {};
			}
		});
	};
  $scope.findFav = function(){

    $state.go('menu.viewSupplier',{favourite_only:true});

  }
  init();
}
]);
