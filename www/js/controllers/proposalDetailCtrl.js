application.controller('proposalDetailCtrl',['$scope','$rootScope','$filter','$stateParams','$ionicModal','$ionicScrollDelegate','$timeout','$ionicHistory','$state','Utils','Offer','ionicToast','config',
function ($scope,$rootScope,$filter,$stateParams,$ionicModal,$ionicScrollDelegate,$timeout,$ionicHistory,$state,Utils, Offer,ionicToast,config) {
  var init = function(hideLoading){
    $scope.options = {show:true};
    $scope.isSupplier = ( $rootScope.user.type == 'Supplier');
    $scope.message = {};
    if($scope.isSupplier){
      supplierInit(hideLoading);
    }else{
      clientInit(hideLoading);
    }

  };
  var supplierInit = function(hideLoading){
    var id = $stateParams.id;
    var offer_promise = Offer.findOffer(id);
    offer_promise.success(function(response){
      $scope.current_offer = response.offer;
      console.log($scope.current_offer);
      if(!$scope.current_offer.my_bid){
        $ionicHistory.currentView($ionicHistory.backView());
        $state.go('menu.jobDetail',{category:$scope.current_offer.category_slug,id:$scope.current_offer.id},{location:'replace'});
      }else{
        $scope.current_proposal = $scope.current_offer.my_bid;
        $scope.navTitle = "Job Detail";
        makeHistory();
      }
    }).error(function(err){
      Utils.hideLoading();
      Utils.alertPopup("Server Error","Try later");
    });
  };

  var clientInit = function(hideLoading){
    $scope.decline = {};
    var id = $stateParams.id;
    if(!hideLoading)
    Utils.showLoading();
    var bid_promose = Offer.getBid(id);
    bid_promose.then(function(response){
      if(response){
        $scope.current_proposal = response.data;
        var offer_id = $scope.current_proposal.offer_id.$oid;
        if($scope.current_proposal.supplier.company_name){
          $scope.navTitle = "Proposal from " + $scope.current_proposal.supplier.company_name;
        }else{
          $scope.navTitle = "Proposal from " + $scope.current_proposal.supplier.first_name + " " + $scope.current_proposal.supplier.last_name;
        }
        return Offer.findOffer(offer_id);
      }
    }, function(err){
      Utils.hideLoading();
      Utils.alertPopup("Server Error","Try later");
    }).then(function(response){
      Utils.hideLoading();
      if(response){
        $scope.current_offer = response.data.offer;
        makeHistory();
      }
    },function(err){
      Utils.hideLoading();
      Utils.alertPopup("Server Error","Try later");
    });
  };
  $scope.dateObject = {
    titleLabel: 'Date',  //Optional
    inputDate: new Date(),  //Optional
    templateType: 'popup', //Optional
    modalHeaderColor: 'bar-dark', //Optional
    modalFooterColor: 'bar-dark', //Optional
    from: new Date(),
    to: new Date(2050,1,1),
    dateFormat: 'dd-MM-yyyy',
    closeOnSelect: true,
    callback: function (val) {
      datePickerCallback(val);
    }
  };
  var datePickerCallback = function (val) {
    if (typeof(val) != 'undefined') {
      $scope.specificDate = new Date(val);
    }
  };
  $scope.openOfferDetailModal = function(){
    $ionicModal.fromTemplateUrl('templates/modals/offerDetail.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.openDeclineProposal = function(){
    $ionicModal.fromTemplateUrl('templates/modals/declineProposal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.declineProposal = function () {
    $scope.updateProposal({
      id: $scope.current_proposal.id,
      status: 'client_cancel',
      message: $scope.decline.message ? $scope.decline.message : $scope.decline.messageText
    });
  };
  $scope.$watch('decline.message', function () {
    if($scope.decline)
    $scope.decline.messageText ="";
  });
  $scope.updateProposal = function (proposal) {
    Utils.showLoading();
    var update_proposal_promise = Offer.updateProposal(proposal);
    update_proposal_promise.success(function(response){
      Utils.hideLoading();
      $scope.current_proposal = response.bid;
      makeHistory();
      $scope.bid="";
      // ionicToast.show(message, position, stick, milisecons); -->
      ionicToast.show(response.message, 'top', false, 3000);
      //$scope.option = {successBox: true,successMessage: response.message};
      if($scope.modal && $scope.modal.isShown()){
        $scope.cancel_modal();
      }
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.alertPopup("Server Error", "Try later");
      if($scope.modal && $scope.modal.isShown()){
        $scope.cancel_modal();
      }
      Utils.debug(err);
    });
  };

  $scope.cancel_modal = function () {
    $scope.modal.remove();
  };
  $scope.openBargain = function(title,status){
    $scope.modalTitle = title;
    $scope.specificDate = new Date();
    $scope.bargain_window_status = status;
    $scope.bid={};
    $ionicModal.fromTemplateUrl('templates/modals/submitProposal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.openDeposit = function(){
    $scope.deposit_option= {choice :'cardPayment'};
    $ionicModal.fromTemplateUrl('templates/modals/deposit.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.accept_proposal = function(){
    confirm_popup_promise = Utils.confirmPopup("Accept Proposal","You want to accept this proposal?","ACCEPT");
    confirm_popup_promise.then(function(res) {
      if(res) {
        $scope.updateProposal({
          id: $scope.current_proposal.id,
          status: 'client_accept',
          message: $scope.decline.message
        });
      }
    });
  };
  $scope.openPaymentDetails = function(){
    Utils.showLoading();
    var payment_promise = Offer.checkout($scope.current_proposal.id);
    payment_promise.success(function(response){
      Utils.hideLoading();
      var inAppBrowserRef = cordova.InAppBrowser.open(response.payment_url, '_blank', 'location=no,hidden=yes');
      inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack);
      inAppBrowserRef.addEventListener('loadstart', loadStartCallBack);
      inAppBrowserRef.addEventListener('loadstop', loadStopCallBack);
      inAppBrowserRef.addEventListener('exit', iabClose);
      function loadStartCallBack(params){
        Utils.showLoading();
      }
      function loadStopCallBack(params){
        Utils.hideLoading();
        inAppBrowserRef.show();
      }
      function loadErrorCallBack(params) {
          console.log(params);
          Utils.debug("ERROR");
          if(params.url.indexOf(config.CLOSE) !== -1 ){
            if($scope.modal && $scope.modal.isShown()){
              $scope.cancel_modal();
            }
            inAppBrowserRef.close();
            $scope.updateProposal({
              id: $scope.current_proposal.id,
              status: 'client_accept',
              message: $scope.decline.message
            });
          }
      }
      function iabClose(event) {
        inAppBrowserRef.removeEventListener('loadstart', loadStartCallBack);
        inAppBrowserRef.removeEventListener('loadstop', loadStopCallBack);
        inAppBrowserRef.removeEventListener('loaderror', loadErrorCallBack);
        inAppBrowserRef.removeEventListener('exit', iabClose);
   }

      }).error(function(err){
        Utils.hideLoading();
        Utils.debug(err);
      });
    }
    $scope.postProposal = function(){
      Utils.showLoading();
      $scope.bid.status = $scope.bargain_window_status;
      $scope.bid.start_date = $scope.specificDate;
      $scope.bid.id = $scope.current_proposal.id;
      $scope.bargain_window_status = "";
      $scope.updateProposal($scope.bid);
    };
    $scope.markDoneClient = function(){
      $scope.rating.client_id = $rootScope.user.id;
      $scope.rating.supplier_id = $scope.current_proposal.supplier_id.$oid;
      Utils.showLoading();
      var rating_promise = Offer.sendRating($scope.current_proposal.id, {rating: $scope.rating});
      rating_promise.success(function(response){
        Utils.hideLoading();
        $scope.updateProposal({
          id: $scope.current_proposal.id,
          status: 'client_complete_approve'
        });
      }).error(function(err){
        Utils.hideLoading();
      });
    }
    $scope.declineDoneClient = function(){
      confirm_popup_promise = Utils.confirmPopup("Not Done","Your job not done?","DECLINE");
      confirm_popup_promise.then(function(res) {
        if(res) {
          $scope.updateProposal({
            id: $scope.current_proposal.id,
            status: 'client_complete_cancel'

          });
        }
      });
    }
    //start of review ana approve modal logic
    $scope.rating = {
      on_time: true,
      response: 'fast',
      feeling: 'happy',
      comment:''
    };

    $scope.setOnTimeValue = function (type) {
      $scope.rating.on_time = type;
    };

    $scope.isActiveOntime = function (type) {
      return type === $scope.rating.on_time;
    };

    $scope.setResponseValue = function (type) {
      $scope.rating.response = type;
    };

    $scope.isActiveResponse = function (type) {
      return type === $scope.rating.response;
    };

    $scope.setFeelingValue = function(type){
      $scope.rating.feeling = type;
    };

    $scope.isFeeling = function(type){
      return type === $scope.rating.feeling;
    };

    $scope.openRatingModal = function(){
      $scope.deposit_option = {};
      $ionicModal.fromTemplateUrl('templates/modals/review_approve.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.sendMessage = function(){
      if($scope.message && $scope.message.content){
        var MESSAGE_JSON = {};
        var clientName = $scope.current_offer.client.first_name + ' ' + $scope.current_offer.client.last_name;
        var supplierName = "";
        if ($scope.current_proposal.supplier.is_company) {
          supplierName = $scope.current_proposal.supplier.company_name;
        } else {
          supplierName = $scope.current_proposal.supplier.first_name + ' ' + $scope.current_proposal.supplier.last_name;
        }

        if($rootScope.user.type.toLowerCase() === 'client'){
          MESSAGE_JSON = {
            message: $scope.message.content,
            bid_id: $scope.current_proposal.id,
            from: {
              name: clientName,
              type: "client"
            },to: {
              name: supplierName,
              type: 'supplier'
            }
          };
        }else{
          MESSAGE_JSON = {
            message: $scope.message.content,
            bid_id: $scope.current_proposal.id,
            to: {
              name: clientName,
              type: "client"
            },from: {
              name: supplierName,
              type: 'supplier'
            }
          };
        }
        var send_sms_promise = Offer.sendMessage(MESSAGE_JSON);
        send_sms_promise.success(function(response){
          MESSAGE_JSON.updated_at = (new Date());
          $scope.completeHistory.push(MESSAGE_JSON);
          if(angular.isNumber($scope.current_proposal.msg_credit) && $scope.current_proposal.msg_credit > 0 ){
            $scope.current_proposal.msg_credit--;
          }
          $scope.message.content = "";
          $ionicScrollDelegate.scrollBottom(true);
        }).error(function(err){
          Utils.debug('Error sending sms',err);
          if(err.message){
            Utils.alertPopup('err.message',err.errors.message[0]);
          }
        })
      }
    };
    var makeHistory = function(){
      console.log($scope.current_proposal);
      var proposalHistory = $scope.current_proposal.prev_versions;
      var current_proposal_data = [{
        amount: $scope.current_proposal.amount,
        amount_per:$scope.current_proposal.amount_per,
        created_at: $scope.current_proposal.created_at,
        currency: $scope.current_proposal.currency,
        message: $scope.current_proposal.message,
        offer_id: $scope.current_proposal.offer_id,
        start_date: $scope.current_proposal.start_date,
        status: $scope.current_proposal.status,
        updated_at:$scope.current_proposal.updated_at,
        id: $scope.current_proposal.id,
        is_current_proposal: true,
        data:$scope.current_proposal.data,
        image:$scope.current_proposal.image?$scope.current_proposal.image:null
      }];
      if(proposalHistory){
        proposalHistory = proposalHistory.concat(current_proposal_data);
        $scope.completeHistory = proposalHistory.concat($scope.current_proposal.messages);
      }else{
        $scope.completeHistory = current_proposal_data.concat($scope.current_proposal.messages);
      }
      $timeout(function () {
        $ionicScrollDelegate.scrollBottom(true);
      }, 200);
      $scope.completeHistory = $filter('orderBy')($scope.completeHistory, 'updated_at');
    };
    $scope.cancelProposal = function(status){
      confirm_popup_promise = Utils.confirmPopup("Cancel Proposal","Are you sure to cancel this proposal?","OK");
      confirm_popup_promise.then(function(res) {
        if(res) {
          Utils.showLoading();
          var promise_update_proposal = Offer.updateProposal({id:$scope.current_proposal.id,status:status});
          promise_update_proposal.success(function(response){
            Utils.hideLoading();
            $scope.current_proposal = response.bid;
            makeHistory();
          }).error(function (error){
            Utils.hideLoading();
            Utils.debug("Unable to delete proposal",error)
            Utils.alertPopup('Unable to delete proposal',error.message);
          });

        }
      });
    };
    $scope.startJobSupplier = function(){
      confirm_popup_promise = Utils.confirmPopup("Start Job","You started your job?","START");
      confirm_popup_promise.then(function(res) {
        if(res) {
          $scope.updateProposal({id:$scope.current_proposal.id,status:'supplier_started'});

        }
      });
    };
    $scope.markDoneSupplier = function(){
      confirm_popup_promise = Utils.confirmPopup("Job Done","Are you done with your job?","DONE");
      confirm_popup_promise.then(function(res) {
        if(res) {
          $scope.updateProposal({id:$scope.current_proposal.id,status:'supplier_completed'});

        }
      });
    };
    $scope.reviewSupplierWindow = function(){
      $scope.review_tags = {'pick':{name:'Picky'},'communicator':{name:'Good Communicator'},'professional':{name:'Professional'},'supporter':{name:'Supporter'},'buyer':{name:'Nice Buyer'}};
      $ionicModal.fromTemplateUrl('templates/modals/submit_review_supplier.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };

    $scope.submitClientReview = function(){
      var selectedTags = [];
      angular.forEach($scope.review_tags,function(tag,key){
        if(tag.selected){
          this.push(key);
        }
      },selectedTags);
      Utils.showLoading();
      var rating_promise = Offer.sendFeedback($scope.current_proposal.id, { feedback:{tags: selectedTags} });
      rating_promise.success(function(response){
        Utils.hideLoading();
        $scope.updateProposal({
          id: $scope.current_proposal.id,
          status: 'supplier_feedback'
        });
      }).error(function(err){
        Utils.debug(err);
        Utils.hideLoading();
      });
    };

    init();



    $scope.$on('new_update', function(e) {
      if($scope.updated_offer_id == $scope.current_offer.id){
        init(true);
      }
    });
  }]);
