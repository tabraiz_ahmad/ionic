application.controller('protectedCtrl',['$rootScope','$scope', '$state', 'Session', function ($rootScope,$scope, $state, Session) {

  //checking user session,
  // also need to implement if connection to server exist
  var init = function(){
    if ($rootScope.user && $rootScope.user.profile_completed) {
      $state.go('menu.landing');
    }else if($rootScope.user && $rootScope.user.type == 'Supplier'){
          $state.go('menu.dashboard');
    }
  }
  $scope.presentScreen = function(type){
      if(type == 'client'){
        var client_present_shown = Session.getItem('client_present_shown');
        if(client_present_shown){
            $state.go('login',{'type':type});
        }else{
            $state.go('presentation',{'type':type});
        }
      }else if(type == 'supplier'){
        var supplier_present_shown = Session.getItem('supplier_present_shown');
        if(supplier_present_shown){
            $state.go('login',{'type':type});
        }else{
            $state.go('presentation',{'type':type});
        }
      }else{
          $state.go('presentation',{'type':type});
      }


  }
  init();
}
]);
