application.controller('reviewCtrl',['$scope','Utils','Client',function($scope,Utils,Client){
  var init = function(){
    $scope.navTitle = 'My Reviews';
    Utils.showLoading();
    var review_promise = Client.myReviews();
    review_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.myReviews = response.reviews;
    })
    .error(function(error){
      Utils.hideLoading();
      Utils.debug('Unable to fetch your review',error);
    });
  }
  init();
}
])
// 5 mint drive from airport 3200 canal december 2016;
