application.controller('serviceCtrl',['$scope','$rootScope','$state','Session','$stateParams','Utils','Offer',function($scope, $rootScope, $state, Session, $stateParams, Utils,Offer){
	var init = function(){

		var category = $stateParams.type;
		var serviceType = Utils.makeTitle(category);
		Utils.showLoading();
		var subcategories_promise = Offer.subcategories(category);
		subcategories_promise
		.success(function(response){
			Utils.hideLoading();
			$scope.subcategories = response;

		})
		.error(function(error){
			Utils.hideLoading();
			Utils.alertPopup("Unable to connect","Try later");
		});
		$scope.navTitle = serviceType;
		$scope.options = {};
  };
	$scope.createOffer = function(category){
		if(category){
				//saving id in offer service and retreving in OfferCtrl
				Offer.category_id  = category.id;
				$state.go('menu.offerMethod',{category:category.slug});
		}
	};
  $scope.showHide = function(event, val){
    $scope.options[val] = $scope.options[val] ? false : true;
    event.stopPropagation();
		if($scope.previous_val && val != $scope.previous_val){
			$scope.options[$scope.previous_val] = false;
		}
		$scope.previous_val = val;
  };

	$scope.showDescription = function(val){
    $scope.options[val] = true;
    if($scope.previous_val && val != $scope.previous_val){
			$scope.options[$scope.previous_val] = false;
		}
		$scope.previous_val = val;
  };
  init();
}
])
