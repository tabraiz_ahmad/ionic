application.controller('supplierBankDetailsCtrl',['$scope','$rootScope','Utils','Supplier','Session', function ($scope, $rootScope, Utils, Supplier,Session) {

  var init = function(){
    $scope.navTitle = "Bank Details";
  }

  $scope.submitDetail = function(){
    Utils.showLoading();
    var supplier_update_promise = Supplier.update($rootScope.user);
    supplier_update_promise.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.supplier);
      $rootScope.go('menu.profile');
    }).error(function(err){
      Utils.hideLoading();
      Utils.alertPopup('Error','Error while saving bank details');

    });
  }
  $scope.cancelHint = function(){
    $scope.hideHint = true;
  }

  init();
}
]);
