application.controller('supplierDocumentsCtrl',['$scope','$rootScope','$q','Utils','Supplier','Session','$ionicPopover','$cordovaCamera','config', function ($scope,$rootScope,$q,Utils,Supplier,Session,$ionicPopover,$cordovaCamera, config) {

  $scope.prefix = config.SERVER_URL;

  var init = function(){
    $scope.navTitle = "Documents";
    $ionicPopover.fromTemplateUrl('templates/popover/selectFilePopover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });
    $scope.hideHint = false;
  }
  $scope.takePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 500,
      targetHeight: 500,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {
      $rootScope.user.document = imageURI;
      $scope.uploadImage();
    }, function (err) {
      // An error occured. Show a message to the user
      Utils.debug('camera error',err);
    });
  }

  $scope.choosePhoto = function () {
    $scope.popover.hide();
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true

    };

    $cordovaCamera.getPicture(options).then(function (imageURI) {
      $rootScope.user.document = imageURI;;
      $scope.uploadImage();
    }, function (err) {
      Utils.debug('image pick error',err);
      // An error occured. Show a message to the user
    });

  };
  $scope.chooseFile = function (fileExt) {
    // iOS (NOTE - only iOS 8 and higher): https://github.com/jcesarmobile/FilePicker-Phonegap-iOS-Plugin
    if (ionic.Platform.isIOS()) {
      FilePicker.pickFile(
        function (uri) {
          $scope.popover.hide();
          chooseFileOk(uri, 'pdf');
        },
        function (error) {
          chooseFileError();
        }
      );
      // Android: https://github.com/don/cordova-filechooser
    } else {
      fileChooser.open(
        function (uri) {
          $scope.popover.hide();
          chooseFileOk(uri, 'pdf');
        },
        function (error) {
          Utils.debug("Error :",error);
          chooseFileError();
        }
      );
    }
  }
  $scope.uploadImage = function(){
    var options = {
      fileKey: "trade_license",
      chunkedMode: false,
      mimeType: "image/jpeg",
      httpMethod: "PUT"
    };
    options.fileName = $rootScope.user.document.substr($rootScope.user.document.lastIndexOf('/') + 1);
    Utils.showLoading();
    var update_suplier_promise = Supplier.uploadImage($rootScope.user,$rootScope.user.document,options);
    update_suplier_promise.then(function(result) {
      Utils.hideLoading();
      var response = JSON.parse(result.response);
      Session.setItemObject('current_user_data',response.supplier);
    }, function(err) {
      Utils.hideLoading();
      Utils.debug("Unable to update image",err);
      user.document = null;
    }, function (progress) {
      // constant progress updates
    });
  }
  $scope.goProfile = function(){
    $rootScope.go('menu.profile');
  }
  $scope.cancelHint = function(){
    $scope.hideHint = true;
  }
  function checkFileType(path, fileExt) {
    return path.match(new RegExp(fileExt + '$', 'i'));
  }

  function chooseFileOk(uri, fileExt) {
    var options = {
      fileKey: "trade_license",
      chunkedMode: false,
      mimeType: "application/pdf",
      httpMethod: "PUT"
    };

    window.FilePath.resolveNativePath(uri, function(localFileUri) {

      options.fileName = localFileUri.substr(localFileUri.lastIndexOf('/') + 1);
      if(localFileUri!= 'undefined' && localFileUri!="" )
      {
        if(localFileUri.match(new RegExp(fileExt + '$', 'i'))){
          Utils.showLoading();
          var update_suplier_promise = Supplier.uploadImage($rootScope.user,localFileUri,options);
          update_suplier_promise.then(function(result) {
            Utils.hideLoading();
            var response = JSON.parse(result.response);
            Session.setItemObject('current_user_data',response.supplier);
          },function(err){
            Utils.hideLoading();
            Utils.debug(err);
          })
        }else{
          Utils.alertPopup("PDF only","Please select PDF file");
        }
      }else{
        Utils.alertPopup("Not Found","Error while fetching file");
      }
    });
  }

  function chooseFileError() {
    Utils.alertPopup("Error","Filepicker not available");
  };
  $scope.isFilePDF = function(){
    if($rootScope.user.trade_license && $rootScope.user.trade_license.url){
      var extn = $rootScope.user.trade_license.url.split(".").pop();
      $scope.fileName = $rootScope.user.trade_license.url.split("/").pop();
      if( extn == 'pdf')
      return true;
    }
    return false;
  };
  $scope.view_pdf = function (url, link_type) {

    if (ionic.Platform.isAndroid()) {
      if (link_type !== undefined && link_type !== null) {
        if (link_type.toLowerCase() !== 'html') {
          url = 'https://docs.google.com/viewer?url=' + encodeURIComponent(url);
        }
      }
    }
    var ref = window.open(url, '_blank', 'location=no');
  };
  $scope.removeTradeLicense = function () {
    var data = {
      remove_trade_license: true,
      id:$rootScope.user.id
    };
    Utils.showLoading();
    var supplier_update_promise = Supplier.update(data);
    supplier_update_promise.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.supplier);
    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("unable to send",error);
      Utils.alertPopup("Error",error.message);
    });
  }
  init();
}
]);
