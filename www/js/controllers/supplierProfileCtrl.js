application.controller('supplierProfileCtrl',['$scope','$stateParams','$state','$ionicModal','Utils','Supplier','$ionicPopup','stringConstants','Offer', function ($scope, $stateParams, $state, $ionicModal, Utils, Supplier,$ionicPopup, stringConstants,Offer) {

  var init = function(){
    var supplier_id = $stateParams.id;
    if(supplier_id){
      findSupplier(supplier_id);
    }
  };
  function findSupplier(supplier_id) {
    $scope.supplier = {};
    $scope.supplier.id = supplier_id;
    $scope.hideBudget = true;
    Utils.showLoading();
    var find_supplier_promise = Supplier.find($scope.supplier);
    find_supplier_promise.success(function(response){
      Utils.hideLoading();
      $scope.supplier = response.supplier;
      serviceNames($scope.supplier);
      areas($scope.supplier.address)
      customerReviews($scope.supplier.id);
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    })
  };
  function serviceNames (supplier){
    $scope.serviceString = supplier.services.map(function(key) {
      return key.name;
    }).join(", ");
    $scope.serviceString  = $scope.serviceString.replace(/,([^,]*)$/,' and$1');
  }
  function customerReviews(id) {
    Utils.showLoading();
    var review_promise = Supplier.reviews(id);
    review_promise.success(function(response){
      Utils.hideLoading();
      $scope.myReviews = response.reviews;
    }).error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
    })
  }
  function areas (address){
    if(address && address.city && address.area){
      if(!address.areas_covering){
        address.areas_covering = [];
      }
      address.areas_covering.push(address.area);
      $scope.areaString = address.areas_covering.map(function(key) {
        return stringConstants.AREAS[address.city][key];
      }).join(", ");
      $scope.areaString  = $scope.areaString.replace(/,([^,]*)$/,' and$1');
    }
  };
  $scope.view_full_image = function (url, link_type) {
    var ref = window.open(url, '_blank', 'location=no');
  };
  $scope.markFavourite = function(){
    Utils.showLoading();
    var mark_fav_promise = Supplier.markFavourite($scope.supplier.id);
    mark_fav_promise
    .success(function(response){
      Utils.hideLoading();
      $scope.supplier = response.supplier;
    })
    .error(function(err){
      Utils.hideLoading();
      Utils.debug(err);
      Utils.alertPopup("Error","Network Error please try later");
    });
  };
  $scope.askService = function() {
    $scope.option = {};
    var popup = $ionicPopup.show({
      'templateUrl': 'templates/popover/service-select.html',
      'title': 'Services',
      scope:$scope,
      'subTitle': 'Please pick a service',
      'buttons': [
        {
          'text': 'Close',
          'type': 'button-light'
        },
        {
          'text': 'Continue',
          'type': 'button-energized',
          'onTap': function(event) {
            if($scope.option.service){
              // console.log($scope.option.service.id);
              Offer.category_id  = $scope.option.service.id;
              $state.go('menu.offer',{category: $scope.option.service.slug,method:'direct',supplier:$scope.supplier.id});
            }
            return $scope.option.service;
          }
        }
      ]
    });
  };

  $scope.openReview = function(){
    $ionicModal.fromTemplateUrl('templates/modals/reviewList.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.cancel_modal = function () {
    if($scope.modal){
      $scope.modal.remove();
    }
  };
  init();
}
]);
