application.controller('supplierServiceAreaCtrl',['$scope','$rootScope','$ionicScrollDelegate','Utils','Supplier','Session','stringConstants', function ($scope, $rootScope,$ionicScrollDelegate, Utils, Supplier,Session,stringConstants) {

  var init = function(){
    $scope.navTitle = "Service Areas";
    $scope.countries=stringConstants.COUNTRIES;
    $scope.cities = stringConstants.CITIES;
    $scope.areas = stringConstants.AREAS;
    $scope.serviceAreas=[];
    if($rootScope.user.address && $rootScope.user.address.areas_covering){
      $scope.serviceAreas =  $rootScope.user.address.areas_covering;
    }
  }

  $scope.submitDetail = function(){
    Utils.showLoading();
    $rootScope.user.address.areas_covering = $scope.serviceAreas;
    var address_attributes = $rootScope.user.address;
    var new_user = $rootScope.user;
    new_user.address_attributes = address_attributes;
    var supplier_update_promise = Supplier.update(new_user);
    supplier_update_promise.success(function(response){
      Utils.hideLoading();
      Session.setItemObject('current_user_data',response.supplier);
      $rootScope.go('menu.profile');
    }).error(function(err){
      Utils.hideLoading();
      Utils.alertPopup('Error','Error while saving service areas');

    });
  }

  $scope.$watch('search_string', function () {
    $ionicScrollDelegate.scrollTop();
  });

  $scope.toggleSelection = function toggleSelection(areaCode) {
    var idx = $scope.serviceAreas.indexOf(areaCode);

    // is currently selected
    if (idx > -1) {
      $scope.serviceAreas.splice(idx, 1);
    }

    // is newly selected
    else {
      $scope.serviceAreas.push(areaCode);
    }

  };
  $scope.cancelHint = function(){
    $scope.hideHint = true;
  }
  $scope.showHint = function(){
    $scope.hideHint = false;
  }
  init();
}
]);
