application.controller('supplierSignUpCtrl',['$scope','$rootScope','$state','Session','Utils','Supplier','$ionicModal','User','$ionicHistory',function($scope, $rootScope, $state, Session, Utils,Supplier,$ionicModal,User,$ionicHistory){
	var init = function(){
		$scope.navTitle = 'Sign Up';
		$scope.user={is_company: true};
		$scope.password = {type:'password',show:false};
		$scope.data = {};
	};

	$scope.showHidepassword = function(){
		if($scope.password.show){
			$scope.password.type = 'password';
			$scope.password.show = false;
		}else{
			$scope.password.type = 'text';
			$scope.password.show = true;
		}
	}
	//verification window
	$scope.verify_num_window = function () {
		$ionicModal.fromTemplateUrl('templates/modals/verify_num.html', function (modal) {
			$scope.verification = modal;
			$scope.verification.show();
		}, {
			scope: $scope
		});
	};
	$scope.confirm_password_key_up = function(user){
		var v = user.password===user.password_confirmation;
		$scope.data.pwError = !v;
	}
	$scope.cancel_verify_num = function(){
		$scope.verification.remove();
	}

	$scope.createSupplier = function(){
		Utils.showLoading();
		var data = $scope.user;
		data.from_mobile = true;
		data.category_ids = Supplier.categories_id;
		var create_supplier_promie = Supplier.create(data);
		create_supplier_promie.success(function(response){
			Utils.hideLoading();
			user = response.supplier;
			Session.setItemObject('current_user_data',user);
			$scope.verify_num_window();

		}).error(function(response){
			Utils.hideLoading();
			angular.forEach(response.errors, function(error,field) {
				Utils.alertPopup('SignUp Error',error);
      })
		});
	}
	$scope.verfiy_user_num = function(){
		var user = Session.getItemObject('current_user_data');
		var id = user.id;

		Utils.showLoading();

		var data = {};
		data.code = $scope.user.code;
		var verify_user_promise = User.verifyCode(id,data);
		verify_user_promise.success(function(response){
			Utils.hideLoading();
			$scope.cancel_verify_num();
			Session.setItemObject('current_user_data',response.user);
			Session.setItemObject('token',response.token);

			if(response.user.profile_completed){
				$scope.go('menu.dashboard');
			}else{
				$scope.go('menu.profile')
			}
		}).error(function(error){
			Utils.hideLoading();
			Utils.debug("num not verified",error);
			Utils.alertPopup("Error",error.message);
		});
	}
	// ********************************************
	// Resend SMS
	$scope.resend_code = function(){

		var user = Session.getItemObject('current_user_data');
		var id = user.id;
		Utils.showLoading();
		var resend_code_promise = User.resendCode(id);
		resend_code_promise.success(function(response){
			Utils.hideLoading();
		}).error(function(error){
			Utils.hideLoading();
			Utils.debug("unable to send",error);
			Utils.alertPopup("Error",error.message);
		});
	}
	// Change phone number
	$scope.change_phone_num = function(){
		$scope.user.code="";
		$scope.user.phone="";
		$scope.cancel_verify_num();

	}

	$scope.go = function (state) {
	    $state.go(state);
	    $ionicHistory.nextViewOptions({
	      disableAnimate: true,
	      disableBack: true
	    });
	  };

	init();
}
])
