application.controller('viewSupplierCtrl',['$scope','$rootScope','$state','$ionicScrollDelegate','$stateParams','$ionicPopup','Utils','User','Supplier','config','Offer',function($scope,$rootScope,$state,$ionicScrollDelegate,$stateParams,$ionicPopup,Utils,User,Supplier,config,Offer){
	$scope.current_page = 1;
	$scope.prefix = config.SERVER_URL;
	function makeInvitedHash() {
		$scope.invited={};
		angular.forEach($scope.offer.supplier_id, function(id) {
			this[id.$oid]=true;
		}, $scope.invited);
	}
	var init = function(){
		$scope.category = $stateParams.category;
		$scope.id = $stateParams.id;
		$scope.repost = $stateParams.repost;
		$scope.favourite_only = $stateParams.favourite_only;
		if($scope.category && $scope.id && $scope.repost){
			$scope.direct = true;
		}
		else if($scope.category && $scope.id){
			$scope.invite = true;
			Utils.showLoading();
			var promise_find_offer = Offer.findOffer($scope.id);
			promise_find_offer.success(function(response){
				Utils.hideLoading();
				$scope.offer = response.offer;
				makeInvitedHash();
			}).error(function (error){
				Utils.hideLoading();
				Utils.alertPopup('Unable to find offer',error.message);
			});

		}else if($scope.category){
			$scope.direct = true;
		}else{
			$scope.normal = true;
		}
		$scope.search_qry = {};
		if($scope.category){

			$scope.search_qry.category_id = $scope.category;
		}
		if($scope.favourite_only){
			$scope.search_qry.favourite_only = true;
		}
		Utils.showLoading();
		var find_supplier_promise = Supplier.supplierList(1,$scope.search_qry);
		find_supplier_promise.success(function(response){
			Utils.hideLoading();
			$scope.supplierList = response.suppliers;
			$scope.total_pages = response.total_pages;
		}).error(function(err){
			Utils.hideLoading();
			Utils.debug(err);
		});
	};

	$scope.loadMore = function(page_num){
		$scope.current_page = page_num;
		Utils.showLoading();
		var find_supplier_promise = $scope.category ? Supplier.supplierList(page_num,$scope.search_qry) : Supplier.findAll(page_num);
		find_supplier_promise
		.success(function(response){
			Utils.hideLoading();
			$scope.supplierList = $scope.supplierList.concat(response.suppliers);
			$scope.total_pages = response.total_pages;
		}).error(function(err){
			Utils.hideLoading();
		});
	};

	$scope.$watch('search_string', function () {
		$ionicScrollDelegate.scrollTop();
	});

	$scope.doRefresh = function() {
		$scope.current_page = 1;
		var find_supplier_promise = $scope.category ? Supplier.supplierList($scope.current_page,$scope.search_qry) : Supplier.findAll($scope.current_page);
		find_supplier_promise
		.success(function(response){
			$scope.supplierList = response.suppliers;
			$scope.total_pages = response.total_pages;
		})
		.finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
		});
	};
	$scope.markFavourite = function(index){
		var id = $scope.supplierList[index].id;
		Utils.showLoading();
		var mark_fav_promise = Supplier.markFavourite(id);
		mark_fav_promise
		.success(function(response){
			Utils.hideLoading();
			$scope.supplierList[index] = response.supplier
		})
		.error(function(err){
			Utils.hideLoading();
			console.log(err);
			Utils.alertPopup("Error","Network Error please try later");
		});
	};
	$scope.createOffer = function(index){
		var id = $scope.supplierList[index].id;
		User.supplier = $scope.supplierList[index];
		if($scope.repost)
		$state.go('menu.offer',{'category': $scope.category,'id':Offer.repostOffer.id,'method':'direct','supplier':id,'repost':true});
		else
		$state.go('menu.offer',{'category': $scope.category,'method':'direct','supplier':id});

	}
	$scope.askService = function(index) {
		$scope.option = {};
		$scope.supplier = $scope.supplierList[index];
		var popup = $ionicPopup.show({
			'templateUrl': 'templates/popover/service-select.html',
			'title': 'Services',
			scope:$scope,
			'subTitle': 'Please pick a service',
			'buttons': [
				{
					'text': 'Close',
					'type': 'button-light'
				},
				{
					'text': 'Continue',
					'type': 'button-energized',
					'onTap': function(event) {
						if($scope.option.service){
							// console.log($scope.option.service.id);
							Offer.category_id  = $scope.option.service.id;
							$state.go('menu.offer',{category: $scope.option.service.slug,method:'direct',supplier:$scope.supplier.id});
						}
						return $scope.option.service;
					}
				}
			]
		});
	};
	$scope.inviteSupplier = function(index){
		var supplier_id = $scope.supplierList[index].id;
		confirm_popup_promise = Utils.confirmPopup("Invite Supplier","You want to invite this supplier?","CONFIRM");
		confirm_popup_promise.then(function(res) {
			if(res) {
				Utils.showLoading();
				var data = {invite:true,offer_id:$scope.id,id:$scope.id,supplier_id:supplier_id};
				var promise_offer_update = Offer.update($rootScope.user.id, data);
				promise_offer_update.success(function(response){
					$scope.offer = response.offer;
					makeInvitedHash();
					Utils.hideLoading();
				}).error(function (error){
					Utils.hideLoading();
					Utils.debug("offer create error",error)
					Utils.alertPopup('Unable to invite',error.message);
				});

			}
		});
	}
	init();
}
])
