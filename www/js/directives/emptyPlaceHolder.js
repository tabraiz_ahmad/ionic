application.directive('emptyPlaceHolder', function () {
  return {
    restrict: 'EA',
    scope: {
      heading: '@heading',
      message: '@message'
    },
    templateUrl: 'templates/directives/emptyPlaceHolder.html'
  }
});
