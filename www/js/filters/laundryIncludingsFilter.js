application.filter('laundryIncludings', function() {
  return function(includings,wear,l_type) {

    var mens_wear = {shirt:"Shirt",suit_2pc:"SUIT 2PC",suit_3pc:"SUIT 3PC",silkshirt:"Silk Shirt",tie:"TIE",overcoat:"OVER COAT",kandurah:"Kandurah",ghutra:"ghutra",wizar_sirwal:"Wizar Sirwal",cap:"Cap",kurta:"Kurta",handkerchief:"Handkerchief",lungi:"Lungi"};
    var ladies_wear = {skirt_pleated:"Skirt Pleated",skirt:"Skirt",blouse:"Blouse",casual_dress:"Casual Dress",saree_abaya:"Saree Abaya",scarf:"Scraf",night_gown:"Night Gown",formal_dress:"Formal Dress",wedding_dress:"Wedding Dress",salwar_khamis_3pc:"Salwar Khamis 3PC",ladies_suit:"Ladies Suit"};
    var other_wear = {sports_pants:"Sports Pants",pants:"Pants",t_shirt:"T-shirt",sweater_pullover:"Sweater Pullover",sports_suit:"Sports Suit",shorts:"Shorts",jacket:"Jacket",underwear:"Underwear",socks_stockings:"Socks Stockings",pyjama_double:"Pyjama Double",bathrobe:"Bathrobe",towel_small:"Towel Small",towel_big:"Towel Big",pillow_case:"Pillow Case",table_cloth_big:"Table Cloth Big",
    bedsheet:"Bedsheet",duvet_comforter:"Duvet Comforter",duvet_cover:"Duvet Cover",cushion_cover_big:"Cushion Cover Big",cushion_cover_small:"Cushion Cover Small",overall:"Overall",apron:"Apron",baby_bottom:"Baby Bottom",baby_top:"Baby Top",shoe_shine:"Shoe Shine"};
    var result = "";
    if(includings){
      if(wear == 'mens_n_ladies_wear'){
        angular.forEach(includings[l_type], function(quantity, name){
          if(mens_wear[name])
          result = result + mens_wear[name] + ' (' + quantity + '),';
          if(ladies_wear[name])
          result = result + ladies_wear[name] + ' (' + quantity + '),';
        });
      }else if (wear == 'other_wear'){
        angular.forEach(includings[l_type], function(quantity, name){
          if(other_wear[name])
          result = result + other_wear[name] + ' (' + quantity + '),';
        });

      }
    }
    if(result === "")
      result = "No Item";
    return result;

  };
});
