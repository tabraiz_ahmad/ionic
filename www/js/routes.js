application.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: '/login/:type',
      cache:false,
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })

    .state('forgotPassword', {
      url: '/forgotPassword',
      cache:false,
      templateUrl: 'templates/forgotPassword.html',
      controller: 'forgotPasswordCtrl'
    })

    .state('resetPassword', {
      url: '/resetPassword',
      cache:false,
      templateUrl: 'templates/resetPassword.html',
      controller: 'forgotPasswordCtrl'
    })

    .state('protected', {
      url: '/protected',
      cache:false,
      templateUrl: 'templates/protected.html',
      controller: 'protectedCtrl'
    })
    .state('presentation', {
      url: '/presentation/:type',
      templateUrl: 'templates/presentation.html',
      controller: 'presentationCtrl'
    })
    .state('menu', {
      url: '',
      templateUrl: 'templates/menu.html',
      cache: false,
      abstract: true,
      controller: 'menuCtrl'
    })
    .state('menu.landing', {
      url: '/landing',
      cache: false,
      views: {
        'main': {
          templateUrl: 'templates/landing.html',
          controller: 'landingCtrl'
        }
      }
    })
    .state('menu.service', {
      url: '/service/:type',
      cache:true,
      views: {
        'main': {
          templateUrl: 'templates/services.html',
          controller: 'serviceCtrl'
        }
      }
    })
    .state('menu.offerMethod', {
      url: '/offerMethod/:category/:repost',
      views: {
        'main': {
          templateUrl: 'templates/offerMethod.html',
          controller: 'offerMethodCtrl'
        }
      }
    })
    .state('menu.offer', {
      cache:true,
      url: '/offer/:category/:method/:id/:supplier/:repost',
      views: {
        'main': {
          templateUrl: 'templates/offer.html',
          controller: 'offerCtrl'
        }
      }
    })
    .state('menu.offerCreatedSuccess', {
      url: '/offerCreatedSuccess',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/offerCreatedSuccess.html',
          controller: 'offerCreatedSuccessCtrl'
        }
      }
    })
    .state('signUp', {
      url: '/signUp',
      cache:false,
      templateUrl: 'templates/supplierSignUp.html',
      controller: 'supplierSignUpCtrl'
    })
    .state('chooseCategory', {
      url: '/chooseCategory',
      cache:false,
      templateUrl: 'templates/chooseCategory.html',
      controller: 'chooseCategoryCtrl'
    })
    .state('menu.jobsList', {
      url: '/jobsList',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/jobsList.html',
          controller: 'jobsListCtrl'
        }
      }
    })
    .state('menu.invitations', {
      url: '/invitations',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/invitations.html',
          controller: 'invitationsCtrl'
        }
      }
    })
    .state('menu.myJobs', {
      url: '/myJobs',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/myJobs.html',
          controller: 'myJobsCtrl'
        }
      }
    })
    .state('menu.dashboard', {
      url: '/dashboard',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/dashboard.html',
          controller: 'dashboardCtrl'
        }
      }
    })
    .state('menu.profile', {
      url: '/profile',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/profile.html',
          controller: 'profileCtrl'
        }
      }
    })
    .state('menu.review', {
      url: '/review',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/review.html',
          controller: 'reviewCtrl'
        }
      }
    })
    .state('menu.editProfile', {
      url: '/editProfile',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/editProfile.html',
          controller: 'editProfileCtrl'
        }
      }
    })
    .state('menu.supplierProfile', {
      url: '/supplierProfile/:id',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/supplierProfile.html',
          controller: 'supplierProfileCtrl'
        }
      }
    })
    .state('menu.supplierDocuments', {
      url: '/documents',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/supplierDocuments.html',
          controller: 'supplierDocumentsCtrl'
        }
      }
    })
    .state('menu.supplierBankDetails', {
      url: '/bankDetails',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/supplierBankDetails.html',
          controller: 'supplierBankDetailsCtrl'
        }
      }
    })
    .state('menu.supplierServiceArea', {
      url: '/serviceArea',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/supplierServiceArea.html',
          controller: 'supplierServiceAreaCtrl'
        }
      }
    })
    .state('menu.jobDetail', {
      url: '/jobDetail/:category/:id',
      cache:false,
      views: {
        'main': {
          templateUrl:  'templates/job-detail.html',
          controller: 'jobDetailCtrl'
        }
      }
    })
    .state('menu.inbox', {
      url: '/inbox',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/inbox.html',
          controller: 'inboxCtrl'
        }
      }
    })
    .state('menu.myOffer',{
      url:'myOffer',
      cache:false,
      views:{
        'main':{
          templateUrl:'templates/myOffer.html',
          controller:'myOfferCtrl'
        }
      }
    })
    .state('menu.myAddresses',{
      url:'/myAddresses',
      cache:false,
      views:{
        'main':{
          templateUrl:'templates/myAddresses.html',
          controller:'myAddressesCtrl'
        }
      }
    })
    .state('menu.proposalDetail', {
      url: '/proposalDetail/:id',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/proposalDetail.html',
          controller: 'proposalDetailCtrl'
        }
      }
    })
    .state('menu.viewSupplier', {
      url: '/viewSupplier/:category/:id/:repost/:favourite_only',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/viewSupplier.html',
          controller: 'viewSupplierCtrl'
        }
      }
    })
    .state('menu.achievment', {
      url: '/achievment',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/achievment.html',
          controller: 'achievementCtrl'
        }
      }
    })
    .state('menu.myPayment', {
      url: '/myPayment',
      cache:false,
      views: {
        'main': {
          templateUrl: 'templates/myPayment.html',
          controller: 'myPaymentCtrl'
        }
      }
    })
    .state('construction', {
      url: '/construction',
      cache:false,
      templateUrl: 'templates/construction.html',
      controller: 'constructionCtrl'
    })
    .state('menu.construction', {
      url: '/menuconstruction',
      cache:false,
      views:{
        'main':{
          templateUrl: 'templates/construction.html',
          controller: 'constructionCtrl'
        }
      }
    })
    .state('payment-details', {
      url: '/payment-details',
      cache:false,
      templateUrl: 'templates/payment-details.html'

    });

    $urlRouterProvider.otherwise('/protected');

}]);
