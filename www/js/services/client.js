application.factory('Client',['$http','config','$cordovaFileTransfer', function($http,config,$cordovaFileTransfer) {
  var service={};

  service.create = function(client) {
    return $http.post(config.API_BASE_PATH + '/clients', { client: client });
  };

  service.update = function(client) {
    return $http.put(config.API_BASE_PATH + '/clients/' + client.id, { client: client });
  };

  service.destroy = function(client) {
    return $http.delete(config.API_BASE_PATH + '/clients/' + client.id);
  };

  service.find = function(client) {
    return $http.get(config.API_BASE_PATH + '/clients/' + client.id);
  };

  service.myReviews = function() {
    return $http.get(config.API_BASE_PATH + '/rating/client');
  };

  service.offers = function(client) {
    return $http.get(config.API_BASE_PATH + '/clients/' + client.id + '/offers');
  };

  service.badges = function() {
    return $http.get(config.API_BASE_PATH + '/badges');
  };

  service.uploadImage = function(client,imageURI,options) {
    return $cordovaFileTransfer.upload(config.API_BASE_PATH + '/clients/' + client.id, imageURI, options);
  };

  return service;
}
]);
