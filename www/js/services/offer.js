application.factory('Offer',['$http','config','$httpParamSerializerJQLike','$cordovaFileTransfer', function($http,config,$httpParamSerializerJQLike,$cordovaFileTransfer) {
  var service={};
  service.category_id = null;
  service.createdOffer = null;
  service.create = function(clientId,offer) {
    return $http.post(config.API_BASE_PATH + '/clients/' + clientId + '/offers', { offer: offer });
  };
  service.update = function(clientId,offer) {
    return $http.put(config.API_BASE_PATH + '/clients/' + clientId + '/offers/' + offer.id, { offer: offer });
  };
  service.findAllCategories = function() {
    return $http.get(config.API_BASE_PATH + '/categories');
  };
  service.findOffer = function(offerId) {
    return $http.get(config.API_BASE_PATH + '/offers/' + offerId );
  };

  service.findSubCategories = function(categoryId) {
    return $http.get(config.API_BASE_PATH + '/categories/' + categoryId + '/subcategories');
  };

  service.allSubCategories = function(categoryId) {
    return $http.get(config.API_BASE_PATH + '/categories/subcategories');
  };

  service.postProposal = function (_proposal) {
    // post proposal details
    return $http.post(config.API_BASE_PATH+'/bids', {'bid': _proposal});
  };

  service.postCarRentalProposal = function(uri,options) {
    console.log(options.params);
    return $cordovaFileTransfer.upload(config.API_BASE_PATH + '/bids', uri, options);
  };

  service.updateProposal = function (_proposal) {
    // post proposal details
    return $http.put(config.API_BASE_PATH +'/bids/'+ _proposal.id,  {bid: _proposal});
  };

  service.offersList = function(num,search_qry) {
    return $http.get(config.API_BASE_PATH + '/offers?page=' +(num ? num:1 )+ '&'+ $httpParamSerializerJQLike(search_qry) );
  };

  service.invitations = function(num) {
    return $http.get(config.API_BASE_PATH + '/offers/invited?page=' +(num ? num:1 ) );
  };

  service.getBid = function(id) {
    return $http.get(config.API_BASE_PATH + '/bids/' +id);
  };
  service.getAllBids = function(id) {
    return $http.get(config.API_BASE_PATH + '/bids');
  };
  service.checkout = function(id) {
    return $http.get(config.API_BASE_PATH + '/bids/' +id + '/checkout?is_mobile=true');
  };
  service.trending = function(){
    return $http.get(config.API_BASE_PATH + '/categories/trending');
  }
  service.subcategories = function(slug){
    if(slug){
      return $http.get(config.API_BASE_PATH + '/categories/' + slug + '/subcategories');
    }else{
      return $http.get(config.API_BASE_PATH + '/categories/subcategories');
    }
  }
  service.delete = function (clientId,offerId) {
    // DELETE
    return $http.delete(config.API_BASE_PATH + '/clients/' + clientId +'/offers/' + offerId);
  };
  service.sendRating = function (bid_id,rating) {
    // send rating
    return $http.post(config.API_BASE_PATH+'/bids/'+bid_id+'/rating', rating);
  };
  service.sendMessage = function (message) {
    // send message
    return $http.post(config.API_BASE_PATH+'/messages',{message:message});
  };
  service.sendFeedback = function (bid_id,feedback) {
    // send feedback
    return $http.post( config.API_BASE_PATH + '/bids/'+ bid_id + '/feedback',feedback);
  };

  service.offerBids = function(num,offerId,search_qry){
    return $http.get(config.API_BASE_PATH + '/offers/'+offerId+'/bids?page=' +(num ? num:1 )+ '&'+ $httpParamSerializerJQLike(search_qry) );

  }
  service.saveJob = function(id){
    return $http.get(config.API_BASE_PATH + '/offers/' + id + '/mark_save');
  };
  service.cancelOffer = function(id){
    return $http.get(config.API_BASE_PATH + '/offers/' + id + '/cancel');
  };
  service.invitedSuppliers = function(id){
    return $http.get(config.API_BASE_PATH + '/offers/' + id + '/invitedsuppliers');
  }
  return service;
}
]);
