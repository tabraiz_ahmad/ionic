application.factory('Search',['$http','config', function($http,config) {
    var service={};

    service.searchList = function(_args) {
      return $http.get(config.API_BASE_PATH + '/search?q='  + _args);
    };
    return service;
}
]);
