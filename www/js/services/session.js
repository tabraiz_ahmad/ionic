application.factory('Session', ['localStorageService','$rootScope',function(localStorageService,$rootScope) {
    var session = {};

    session.setItem = function(key,value){
        localStorageService.set(key,value);
    };

    session.getItem = function(key){
        return localStorageService.get(key);
    };

    session.setItemObject = function(key,value){
        if(key=="current_user_data"){
          $rootScope.user =  value;
        }

        localStorageService.set(key,JSON.stringify(value));
    };

    session.getItemObject = function(key){
        return JSON.parse(localStorageService.get(key));
    };

    session.removeItem = function(key){
        localStorageService.remove(key);
    };

    session.clearSession = function(){
        localStorageService.clearAll();
    };

    session.isAuthenticated = function(){
        if(session.getItem('current_user_data')){
            return true;
        }else{
            return false;
        }
    };

    return session;
}
]);
