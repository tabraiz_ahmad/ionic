application.factory('Supplier',['$http','$cordovaFileTransfer','config','$httpParamSerializerJQLike', function($http,$cordovaFileTransfer,config,$httpParamSerializerJQLike) {
    var service={};
    service.categories_id = null;
    service.create = function(supplier) {
      return $http.post(config.API_BASE_PATH + '/suppliers', { supplier: supplier });
    };

    service.update = function(supplier) {
      return $http.put(config.API_BASE_PATH + '/suppliers/' + supplier.id, { supplier: supplier });
    };
    service.uploadImage = function(supplier,uri,options) {
      return $cordovaFileTransfer.upload(config.API_BASE_PATH + '/suppliers/' + supplier.id, uri, options);
    };

    service.destroy = function(supplier) {
      return $http.delete(config.API_BASE_PATH + '/suppliers/' + supplier.id);
    };

    service.find = function(supplier) {
      return $http.get(config.API_BASE_PATH + '/suppliers/' + supplier.id);
    };
    service.supplierList = function(num,search_qry) {
      return $http.get(config.API_BASE_PATH + '/suppliers?page=' +(num ? num:1 )+ '&'+ $httpParamSerializerJQLike(search_qry) );
    };

    service.findAll = function(num) {
      return $http.get(config.API_BASE_PATH + '/suppliers?page=' + ( num ? num:1) );
    };
    service.findAllFavourites = function(num) {
      return $http.get(config.API_BASE_PATH + '/suppliers?favourite_only=true&page=' + ( num ? num:1) );
    };
    service.markFavourite = function(id){
      return $http.get(config.API_BASE_PATH + '/suppliers/' + id + '/mark_favourite');
    };
    service.myJobs = function(id,qry) {
      return $http.get(config.API_BASE_PATH + '/suppliers/'+id+'/my_jobs'+'?filter='+qry );
    };
    service.reviews = function(id) {
      return $http.get(config.API_BASE_PATH + '/reviews/'+id );
    };

    service.earnings = function(id) {
      return $http.get(config.API_BASE_PATH + '/transactions/goal');
    };
    service.servicetool = function(data) {
      return $http.post(config.API_BASE_PATH + '/servicetools', { servicetool: data });
    };

    return service;
}
]);
