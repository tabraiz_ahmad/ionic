application.factory('Transaction',['$http','config','$httpParamSerializerJQLike', function($http,config,$httpParamSerializerJQLike) {
    var service={};
    service.find = function(search_qry) {
      return $http.get(config.API_BASE_PATH + '/transactions?'+ $httpParamSerializerJQLike(search_qry) );
    };
    service.download = function(transaction_id) {
      return $http.get(config.API_BASE_PATH + '/transactions/'+ transaction_id + '/download' );
    };

    return service;
}
]);
