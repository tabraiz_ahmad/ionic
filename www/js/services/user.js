application.factory('User',['$http','config', function($http,config) {
    var service={};
    service.auth = function(type,provider,auth_token) {
      return $http.post(config.API_BASE_PATH + '/users/auth/' + type + '/' + provider,{'access_token': auth_token});
    };
    service.signIn = function(data) {
      return $http.post(config.API_BASE_PATH + '/users/sign_in', data);
    };
    service.forgotPasswordEmail = function(data) {
      return $http.post(config.API_BASE_PATH + '/users/forgot_password',data);
    };
    service.resetPassword = function(data) {
      return $http.post(config.API_BASE_PATH + '/users/reset_password',data);
    };
    service.signOut = function() {
      return $http.get(config.API_BASE_PATH + '/users/sign_out');
    };
    service.update = function(data) {
        return $http.put(config.API_BASE_PATH + '/users/update', data);
      };
    service.changePassword = function(data) {
        return $http.put(config.API_BASE_PATH + '/users/change_password', data);
    };
    service.sendPushToken = function(data) {
        return $http.put(config.API_BASE_PATH + '/users/pns_token', data);
    };
    service.verifyCode = function(id, data) {
      return $http.post(config.API_BASE_PATH + '/users/' + id + '/verify_code', data);
    };

    service.notification = function(num) {
      return $http.get(config.API_BASE_PATH + '/notifications?page='+ (num?num:1));
    };
    service.markmsgAsRead = function(notification_id){
      return $http.get(config.API_BASE_PATH + '/notifications/'+ notification_id);
    }
    service.resendCode = function(id) {
      return $http.get(config.API_BASE_PATH + '/users/' + id + '/resend_code');
    };
    service.updatePassword = function (user) {
      return $http.post(config.API_BASE_PATH + '/users/change_password', user);
    };
    return service;
}
]);
