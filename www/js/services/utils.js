application.factory('Utils',['$ionicPopup','config','$ionicLoading','$rootScope', function ($ionicPopup,config, $ionicLoading,$rootScope) {
  var utils = {};


  utils.is_mobile_ua = function () {
    return /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent);
  };
  utils.userAgent = function(){
    console.log(navigator.userAgent);
    console.log(navigator);
  }
  // ********************************************
  utils.is_ipad = function () {
    return (navigator.userAgent.match(/iPad/i) !== null);
  };

  // ********************************************
  utils.is_iphone = function () {
    return (navigator.userAgent.match(/iPhone/i) !== null);
  };

  // ********************************************
  utils.is_ipod = function () {
    return (navigator.userAgent.match(/iPod/i) !== null);
  };

  // ********************************************
  utils.is_ios = function () {
    return (is_ipad() || is_iphone() || is_ipod());
  };

  // ********************************************
  utils.is_android = function () {
    return (navigator.userAgent.match(/Android/i) !== null);
  };

  // ********************************************
  utils.get_android_version = function () {
    if (is_android()) {
      var match = navigator.userAgent.match(/Android\s([0-9\.]*)/);
      return parseFloat(match[1]);
    }
    return false;
  };
  // ********************************************

//   {
//   title: '', // String. The title of the popup.
//   cssClass: '', // String, The custom CSS class name
//   subTitle: '', // String (optional). The sub-title of the popup.
//   template: '', // String (optional). The html template to place in the popup body.
//   templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
//   cancelText: '', // String (default: 'Cancel'). The text of the Cancel button.
//   cancelType: '', // String (default: 'button-default'). The type of the Cancel button.
//   okText: '', // String (default: 'OK'). The text of the OK button.
//   okType: '', // String (default: 'button-positive'). The type of the OK button.
// }

// cssClass:'white-background zero-border',
// cancelText: 'CANCEL',
// cancelType:'button-clear button-energized'

  // A confirm dialog, returing promise
  utils.confirmPopup = function(title, message,okayText) {
    var confirm_popup_promise = $ionicPopup.confirm({
      cssClass: (title ? 'zero-border': 'no-title zero-border padding'),
      title: title,
      template: '<p class="text-16">' + message + '</p>',
      okText: (okayText?okayText:"CONFIRM"),
      okType: 'button-clear button-energized',
      cancelType:'button-clear button-energized',
      cancelText:'CANCEL'

    });
    return confirm_popup_promise;
  };

  // ********************************************
  utils.alertPopup = function(title, message){
    var alert_popup = $ionicPopup.alert({
      title: title,
      template: message
    });
    return alert_popup;

  }

  // ********************************************
  utils.showLoading = function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>',
      hideOnStateChange:true
    });
  }

  // ********************************************
  //options: {
  //   template: '<ion-spinner icon="ripple"></ion-spinner>',
  //   hideOnStateChange:true
  // }
  utils.showLoadingWithOptions = function(options){
    $ionicLoading.show(options);
  }
  // ********************************************
  utils.hideLoading = function(){
    $ionicLoading.hide();
  }

  // ********************************************
  utils.debug = function () {
    var args = arguments; // allow comma separated debug
    // messages

    // output messages to console
    if (config.ENVIROMENT === 'dev') {
      angular.forEach(args, function (msg, key) {
        console.log(msg);
      });
    }
  };
  utils.isVerified = function(){
      if($rootScope.user.active)
        return true;
      return false;
  }

  utils.makeTitle = function(slug) {
      var words = slug.split('-');

      for(var i = 0; i < words.length; i++) {
        var word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1);
      }

      return words.join(' ');
  }

  var prependZero = function (param) {
    if (String(param).length < 2) {
      return "0" + String(param);
    }
    return param;
  };

  utils.epochParser = function (val, opType) {
    if (val === null) {
      return "00:00";
    } else {
      var meridian = ['AM', 'PM'];

      if (opType === 'time') {
        var hours = parseInt(val / 3600);
        var minutes = (val / 60) % 60;
        var hoursRes = hours > 12 ? (hours - 12) : hours;

        var currentMeridian = meridian[parseInt(hours / 12)];

        return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
      }
    }
  };

  return utils;
}
]);
